//
//  UserViewModel.swift
//  KidsSocial
//
//  Created by cql131 on 21/06/23.
//

import UIKit

class UserViewModel: NSObject {
    func userHomeApicall(latitude:Double,longitude:Double,onsuccess:@escaping((UserHomeModelBody?)->())){
//30.7046° N, 76.7179° E
        let param: parameters = ["latitude":latitude, "longitude":longitude]
        print("param========",param)
        WebService.service(API.user_home, param: param,service: .post) {
            (modaldata: UserHomeModel, data , json) in
            onsuccess(modaldata.body)
    }
        
    }
    //MARK: - BUSINESS SEARCH
    func buisnessGetApicall(onsuccess: @escaping (([BuisnessGetModelBody]?)->())){
        WebService.service(API.business_listing, service: .get) {
            (modaldata: BuisnessGetModel, data , json) in
            onsuccess(modaldata.body)
    }
    }
    //MARK: - DETAILS BUSINESS
    func favouriteListing(onsuccess: @escaping (([FavListingtModelBody]?)->())){
        WebService.service(API.favourite_listing, service: .get) {
            (modaldata: FavListingtModel, data , json) in
            onsuccess(modaldata.body)
    }
    }
    //MARK: - SUB CATEGORY LISTING
    func subcategoryListingapicall(category_id: Int,sub_category_id:Int,onsuccess: @escaping (([SubBusinessModelBody]?)->())){
        let param: parameters = ["category_id":category_id, "sub_category_id":sub_category_id]
        print("param========",param)
        WebService.service(API.category_related_business, param: param,service: .post) {
            (modaldata: SubBusinessModel, data , json) in
            onsuccess(modaldata.body)
    }
    }
    
}
