//
//  AuthviewModel.swift
//  KidsSocial
//
//  Created by cql131 on 13/06/23.
//

import UIKit
import SwiftUI

class AuthviewModel: NSObject {

//    MARK: - SIGNUP API
     func signupApicall(name:String, email:String, phone:String, password:String, confirmpassword:String,role:Int, onSuccess: @escaping((SignInModel)->())){
        if name.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter name", isSuccess: .error)
        }else  if email.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter email", isSuccess: .error)
        }else if email.isValidEmail{
            CommonUtilities.shared.showAlert(message: "Please enter valid email", isSuccess: .error)
        }
        else if phone.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter mobile", isSuccess: .error)
        }else if phone.count <= 9{
            CommonUtilities.shared.showAlert(message: "Please enter phone number minimum 10 characters", isSuccess: .error)
        }
        else if password.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter password", isSuccess: .error)
        }else if password.count <= 5{
            CommonUtilities.shared.showAlert(message: "Please enter password minimum 6 characters", isSuccess: .error)
        }else if confirmpassword.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter confirm password", isSuccess: .error)
        }else if password != confirmpassword{
            CommonUtilities.shared.showAlert(message: "Password and confirm password should be same", isSuccess: .error)
        }else{
            let param: parameters = ["name":name,
                                     "email":email,
                                     "phone":phone,
                                     "password":password,
                                     "role":role,"device_token":DEVICE_TOKEN]
            print(param)
            WebService.service(API.signup,  param: param, service: .post){
                (modeldata: SignInModel, data, json) in
                Store.userDetails = modeldata
                Store.sociallogin = false
                Store.authKey = modeldata.body?.token ?? ""
                onSuccess(modeldata)
            }
        }
}

    //MARK: - LOGIN API
    func loginApicall(email:String, password:String,role:Int, onSuccess: @escaping ((SignInModelBody?)->())) {

                let param: parameters = ["email":email,
                                         "password":password,"device_token":DEVICE_TOKEN, "role":role]
                print(param)
                
                WebService.service(API.login, param: param, service: .post){
                    (modaldata: SignInModel , Data, Json) in
                    Store.userDetails = modaldata
                    Store.authKey = modaldata.body?.token ?? ""
                    Store.autoLogin = true
                    onSuccess(modaldata.body)
                }
//            }
        }
    

    //MARK: - RESET PASSWORD
    func emailApicall(email:String, onSuccess: @escaping (()->())){
        if email.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter email", isSuccess: .error)
        }else if email.isValidEmail{
            CommonUtilities.shared.showAlert(message: "Please enter valid email", isSuccess: .error)
        }
    }
    
//    MARK: - VERIFICATION OTP
    func otpverification(otp:String, onSuccess :@escaping()->()){
        if otp.count < 4 {
            CommonUtilities.shared.showAlert(message: "Please enter otp", isSuccess: .error)
        }else{
            let param : parameters = ["otp":otp]
            print(param)
            WebService.service(API.verify_otp, param: param, service: .post){
                (modaldata : VerifyOtpModel , data, json) in
                onSuccess()
        }

    }
    }
    //MARK: RESEND OTP
    func ResendOtp(email: String, onSuccess : @escaping ((ResendOTPBody?)->())){
        let param: parameters = ["email" : email]
        print(param)
        WebService.service(API.resend_otp, param: param, service: .post) {
            (modaldata: ResendOTPModel , Data, Json) in
            onSuccess(modaldata.body)
        }
    }
    //MARK: Forgot Password
    func ForgotPassword(email: String, onSuccess : @escaping (()->())){
        if  email.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter  email", isSuccess: .error)
        }else{
            let param: parameters = ["email" : email]
            print(param)
            WebService.service(API.forgot_password, param: param, service: .post) {
                (modaldata: resetpasswordModel , Data, Json) in
                onSuccess()
            }
        }
    }
    
    //MARK: - EDIT PROFILE
    func editprofileapicall(name:String, mobile:String, image:UIImage,   onSuccess : @escaping ((edirprofilebody?)->())){
        //IMAGE: -
        if name.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter name", isSuccess: .error)
        }else if mobile.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter mobile number", isSuccess: .error)
        }else{
            let formatter = DateFormatter()
            formatter.dateFormat = dateFormat.fullDate.rawValue
            let date = formatter.string(from: Date())
            let imageInfo : ImageStructInfo
         imageInfo = ImageStructInfo.init(fileName: "Img\(date).jpg", type: "image/jpg", data: image.toData() ?? Data(), key: "image", image: image)
            let param:  [String:Any] =  ["name":name,
                                         "phone":mobile,
                                         "image":imageInfo]
            print(param)
            WebService.service(API.edit_profile, param: param , service: .post ){
                (modaldata: editprofilemodel , Data, json) in
            onSuccess(modaldata.body)
            }
        }
   
    }
    
    //MARK: - ABOUT US
    func aboutUsApi(onSuccess : @escaping ((AboutusModelbody?)->())){
        WebService.service(API.aboutUs ,service: .get, showHud: true) {
            (modaldata: AboutusModel , Data, Json) in
            onSuccess(modaldata.body)
        }
    }

    //MARK: - PRIVACY POLICY
    func privacypolicyApi(onSuccess : @escaping ((AboutusModelbody?)->())){
        WebService.service(API.privacypolicy ,service: .get, showHud: true) {
            (modaldata: AboutusModel , Data, Json) in
            onSuccess(modaldata.body)
        }
    }
    //MARK: - TEARMS & CONDITION
    func termsandconditionApi(onSuccess : @escaping ((AboutusModelbody?)->())){
        WebService.service(API.termsAndCondition ,service: .get, showHud: true) {
            (modaldata: AboutusModel , Data, Json) in
            onSuccess(modaldata.body)
        }
    }

    
//  MARK: - CHANGE PASSWORD
    func changePasswordapicall(oldpassword: String, newpassword: String, confirmpassword: String, onsuccess: @escaping (()->())){
         if oldpassword.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter old password", isSuccess: .error)
        }else if oldpassword == newpassword{
            CommonUtilities.shared.showAlert(message: "Old password and new password should not be same", isSuccess: .error)
        }else if newpassword.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter new password", isSuccess: .error)
        }else if newpassword.count <= 5{
            CommonUtilities.shared.showAlert(message: "Please enter password minimum 6 characters", isSuccess: .error)
        }else if confirmpassword.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter confirm  password", isSuccess: .error)
        }else if confirmpassword != newpassword{
            CommonUtilities.shared.showAlert(message: "New password and confirm password should be same", isSuccess: .error)
        }else{
            let param : parameters = ["old_password":oldpassword,
                                      "new_password":newpassword,
                                      "confirm password":confirmpassword]
            print(param)
            WebService.service(API.change_password, param: param, service: .post){
                (modaldata : VerifyOtpModel , data, json) in
                onsuccess()
        }
        }
 
    }
  
//    MARK: - LOGOUT API CALL
    func logoutapicall(onsuccess: @escaping (()->())){
        WebService.service(API.logout, service: .post) {
            (modaldata: VerifyOtpModel, Data , json) in
            onsuccess()
        }
        
    }
    
    //MARK: - GET PROFILE
    func getprofile(onsuccess: @escaping ((getProfileModalBody?)->())){
        WebService.service(API.get_profile, service: .get) {
            (modaldata: getProfileModal, Data , json) in
            onsuccess(modaldata.body)
        }
    }
    //MARK: - SOCIAL LOGIN
    func SocialLogin(socialType:String , socialId:String,name:String ,email:String , onsuccess: @escaping (()->())){
        let param : parameters = ["social_type":socialType,
                                  "social_id":socialId,
                                  "name":name,
                                  "email":email,
                                  "device_type":1,
                                  "device_token":DEVICE_TOKEN,
                                  "role": "1"]
        print(param)
        WebService.service(API.social_login, param: param, service: .post){
            (modeldata: SignInModel , Data, json) in
            Store.authKey = modeldata.body?.token ?? ""
            Store.autoLogin = true
            //            Store.sociallogin = true
            Store.userDetails = modeldata
            onsuccess()
        }
    }
    //    MARK: -  DELETE ACCOUNT
    func deleteuserApi(onsuccess:@escaping (()->())){
        let param : parameters = ["user_id":Store.userDetails?.body?.id ?? ""]
        WebService.service(API.delete_user, param: param,service: .post) {
            (modaldata: DeleteAccount , Data, Json) in
            Store.userDetails = nil
            Store.autoLogin = false
            onsuccess()
        }
    }
}
