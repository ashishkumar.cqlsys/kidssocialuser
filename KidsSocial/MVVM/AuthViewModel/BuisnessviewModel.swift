//
//  BuisnessviewModel.swift
//  KidsSocial
//
//  Created by cql131 on 15/06/23.
//

import UIKit
import SwiftUI

class BuisnessviewModel: NSObject {
    
    //MARK: - GET CATEGORY
    func categoryapicall(onsuccess: @escaping (([CategoryModelBody]?)->())){
        WebService.service(API.category_listing, service: .get) {
            (modaldata: CategoryModel, Data , json) in
            onsuccess(modaldata.body)
        }
    }
    //MARK: - sub category
    func subcategoryapicall(categoryId: Int,onsuccess: @escaping (([SubcategoryModelBody]?)->())){
        let param  = ["categoryId": categoryId]
        print(param)
        WebService.service(API.sub_categories_listing, param: param, service: .post) {
            (modaldata: SubcategoryModel, Data , json) in
            onsuccess(modaldata.body)
        }
    }
    //    MARK: - ADD BUISNESS
    func addbuisness(isImageSelected:Bool,isVideoSelect:Bool,name:String, phone:String, short_description:String, location:String, latitude:Double, longitude:Double, image:UIImage, category_id: Int, sub_category_id: Int, video:ImageStructInfo? ,onsuccess: @escaping (()->())){
        if  !(isImageSelected) {
            CommonUtilities.shared.showAlert(message: "Please select image ", isSuccess: .error)
        }
        else if !(isVideoSelect) {
            CommonUtilities.shared.showAlert(message: "Please select video ", isSuccess: .error)
        }
        else
        if name.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter business name", isSuccess: .error)
        }else if category_id == 0{
            CommonUtilities.shared.showAlert(message: "Please select category", isSuccess: .error)
        }else if sub_category_id == 0{
            CommonUtilities.shared.showAlert(message: "Please select sub category", isSuccess: .error)
        }else if phone.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter phone", isSuccess: .error)
        }else if phone.count <= 9{
                CommonUtilities.shared.showAlert(message: "Please enter phone number minimum 10 characters", isSuccess: .error)
          
        }else if location.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter location", isSuccess: .error)
        }else if short_description.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter short description", isSuccess: .error)
        }else{
            let formatter = DateFormatter()
            formatter.dateFormat = dateFormat.fullDate.rawValue
            let date = formatter.string(from: Date())
            let imageInfo : ImageStructInfo
            imageInfo = ImageStructInfo.init(fileName: "Img\(date).jpg", type: "image/jpg", data: image.toData() ?? Data(), key: "image", image: image)
            let param: parameters = ["name":name,
                                     "phone":phone,
                                     "short_description":short_description,
                                     "location":location,
                                     "latitude":latitude,
                                     "longitude":longitude,
                                     "image":imageInfo ,
                                     "category_id":category_id,
                                     "sub_category_id":sub_category_id, "video":video]
            print(param)
            WebService.service(API.add_business, param: param , service: .post){
                (modeldata: AddBuisnessModel, data, json) in
                onsuccess()
            }
        }
    }
    
    //    MARK: - EDIT BUSINESS
    func editbuisness(name:String, phone:String, short_description:String, location:String, latitude:Double, longitude:Double, image:UIImage, category_id: Int, sub_category_id: Int, business_id:Int,video:ImageStructInfo ,onsuccess: @escaping (()->())){
        let formatter = DateFormatter()
        formatter.dateFormat = dateFormat.fullDate.rawValue
        let date = formatter.string(from: Date())
        let imageInfo : ImageStructInfo
        imageInfo = ImageStructInfo.init(fileName: "Img\(date).jpg", type: "image/jpg", data: image.toData() ?? Data(), key: "image", image: image)
        let param: parameters = ["name":name,
                                 "phone":phone,
                                 "short_description":short_description,
                                 "location":location,
                                 "latitude":latitude,
                                 "longitude":longitude,
                                 "image":imageInfo ,
                                 "category_id":category_id,
                                 "business_id":business_id,
                                 "sub_category_id":sub_category_id, "video":video]
        print(param)
        WebService.service(API.edit_business, param: param , service: .post){
            (modeldata: EditbusinessModel, data, json) in
            onsuccess()
        }
    }
    //MARK: - BUSINESS HOME
    func buisnessHomeApiCall(onsuccess: @escaping ((BusinessHomeModelBody?)->())){
        WebService.service(API.Business_home, service: .get) {
            (modaldata: BusinessHomeModel, data , json) in
            onsuccess(modaldata.body)
        }
    }
    
    //    MARK: - BUSINESS DETAILS
    func buisnessDetailsapicall(business_id:Int, onsuccess: @escaping((BusinessDetailsModelBody?)->())){
        
        let param :parameters = ["business_id":business_id]
        print (param)
        WebService.service(API.business_detail, param: param , service: .post){
            (modeldata: BusinessDetailsModel, Data, json) in
            onsuccess(modeldata.body)
            
        }
    }
    //   MARK: - BUSINESS DELETE
    func businessdalete(business_id: Int, onsuccess: @escaping((_ getData: DeleteBusinessModelBody?)->())){
        let param : parameters = ["business_id":business_id]
        print(param)
        WebService.service(API.delete_business, param: param , service: .post){
            (modeldata: DeleteBusinessModel, Data, json) in
            onsuccess(modeldata.body)
        }
    }
    
    //    MARK: - FAV UNFAV BUSINESS
    func favUnfav(status: Int, business_id: Int, onsuccess: @escaping(()->())){
        let param: parameters = ["type":status,
                                 "business_id":business_id]
        print(param)
        WebService.service(API.favourite_unfavourite, param: param , service: .post){
            (modeldata: FavoriteModel, Data, json) in
            onsuccess()
        }
    }
    //    MARK: - ACTIVE INACTIVE BUSINESS
    func activeinactiveapicall(status: Int, business_id: Int, onsuccess: @escaping((likedislikebody?)->())){
        let param: parameters = ["status":status,
                                 "business_id":business_id]
        print(param)
        WebService.service(API.active_inactive_business, param: param , service: .post){
            (modeldata: likedislike, Data, json) in
            onsuccess(modeldata.body)
        }
    }
    //    MARK: - SEARCH BUSINESS
    func searchBusinessAPIcall(name:String, onsuccess: @escaping((SearchBusinessModel)->())){
        let param: parameters = ["name":name]
        print(param)
        WebService.service(API.search_business, param: param , service: .post){
            (modeldata: SearchBusinessModel, Data, json) in
            onsuccess(modeldata)
        }
    }
    func notificationListingApiCall(onsuccess: @escaping (([NotificationListingModelBody]?)->())){
        WebService.service(API.notification_listing, service: .get) {
            (modaldata: NotificationListingModel, data , json) in
            onsuccess(modaldata.body)
        }
    }
    func usernotificationListingApiCall(onsuccess: @escaping (([userNotificationModelBody]?)->())){
        WebService.service(API.get_notification_listing_user, service: .get) {
            (modaldata: userNotificationModel, data , json) in
            onsuccess(modaldata.body)
        }
    }
}


