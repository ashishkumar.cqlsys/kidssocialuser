//
//  DeleteBusinessModel.swift
//  KidsSocial
//
//  Created by cql131 on 21/06/23.
//

import Foundation

// MARK: - DeleteBusinessModel
struct DeleteBusinessModel: Codable {
    var success: Bool?
    var code: Int?
    var message: String?
    var body: DeleteBusinessModelBody?
}

// MARK: - Body
struct DeleteBusinessModelBody: Codable {
}
