//
//  likedislikemodel.swift
//  KidsSocial
//
//  Created by cql131 on 26/06/23.
//

import Foundation

// MARK: - UserHomeModel
struct likedislike: Codable {
    var success: Bool?
    var code: Int?
    var message: String?
    var body: likedislikebody?
}

// MARK: - Body
struct likedislikebody: Codable {
}

