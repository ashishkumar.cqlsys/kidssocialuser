//
//  SubCategoryModel.swift
//  KidsSocial
//
//  Created by cql131 on 15/06/23.
//

//
//import Foundation
//
//// MARK: - SubcategoryModel
//struct SubcategoryModel: Codable {
//    var success: Bool?
//    var code: Int?
//    var message: String?
//    var body: SubcategoryModelBody?
//}
//
//// MARK: - SubcategoryModelBody
//struct SubcategoryModelBody: Codable {
//    var id: Int?
//    var name, image: String?
//    var status: Bool?
//    var createdAt, updatedAt: String?
//    var subcat: [Subcat]?
//}
//
//// MARK: - Subcat
//struct Subcat: Codable {
//    var id, catID: Int?
//    var subcatName, image: String?
//    var status: Bool?
//    var createdAt, updatedAt: String?
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case catID = "cat_id"
//        case subcatName = "subcat_name"
//        case image, status, createdAt, updatedAt
//    }
//}


import Foundation

// MARK: - SubCategory
struct SubcategoryModel: Codable {
    var success: Bool?
    var code: Int?
    var message: String?
    var body: [SubcategoryModelBody]?
}

// MARK: - Body
 struct SubcategoryModelBody: Codable {
    var id, catID: Int?
    var subcatName, image: String?
    var status: Bool?
    var createdAt, updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id
        case catID = "cat_id"
        case subcatName = "subcat_name"
        case image, status, createdAt, updatedAt
    }
}
