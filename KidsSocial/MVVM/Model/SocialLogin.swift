//
//  SocialLogin.swift
//  KidsSocial
//
//  Created by cqlpc on 03/07/23.
//

//import Foundation
//
//// MARK: - SocialLogin
//struct SocialLogin: Codable {
//    var success: Bool
//    var code: Int
//    var message: String
//    var body: SocialLoginbody
//}
//
//// MARK: - Body
//struct SocialLoginbody: Codable {
//    var id: Int
//    var name, email, phone, countryCode: String
//    var image: String
//    var role, otpVerified: Int
//    var password: String
//    var status: Int
//    var location, longitude, latitude, profileImg: String
//    var otp: Int
//    var deviceType, deviceToken, socialID: String
//    var socialType: Int
//    var createdAt, updatedAt, token: String
//
//    enum CodingKeys: String, CodingKey {
//        case id, name, email, phone
//        case countryCode = "country_code"
//        case image, role
//        case otpVerified = "otp_verified"
//        case password, status, location, longitude, latitude
//        case profileImg = "profile_img"
//        case otp
//        case deviceType = "device_type"
//        case deviceToken = "device_token"
//        case socialID = "social_id"
//        case socialType = "social_type"
//        case createdAt, updatedAt, token
//    }
//}

import Foundation

// MARK: - Welcome
struct SocialLogin: Codable {
    var success: Bool
    var code: Int
    var message: String
    var body: SocialLoginbody
}

// MARK: - Body
struct SocialLoginbody: Codable {
    var id: Int
    var name, email, phone, countryCode: String
    var image: String
    var role, otpVerified, isReset: Int
    var password: String
    var status: Int
    var location, longitude, latitude, profileImg: String
    var otp: Int
    var deviceType, deviceToken, socialID: String
    var socialType: Int
    var createdAt, updatedAt, token: String

    enum CodingKeys: String, CodingKey {
        case id, name, email, phone
        case countryCode = "country_code"
        case image, role
        case otpVerified = "otp_verified"
        case isReset, password, status, location, longitude, latitude
        case profileImg = "profile_img"
        case otp
        case deviceType = "device_type"
        case deviceToken = "device_token"
        case socialID = "social_id"
        case socialType = "social_type"
        case createdAt, updatedAt, token
    }
}
