//
//  userNotificationListModel.swift
//  KidsSocial
//
//  Created by cqlpc on 11/07/23.
//


//import Foundation
//
//// MARK: - Welcome
//struct userNotificationModel: Codable {
//    var success: Bool
//    var code: Int
//    var message: String
//    var body: [userNotificationModelBody]
//}
//
//// MARK: - Body
//struct userNotificationModelBody: Codable {
//    var id, userID, businessID: Int
//    var businessName, message: String
//    var status, type: Int
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case userID = "user_id"
//        case businessID = "business_id"
//        case businessName = "business_name"
//        case message, status, type
//    }
//}

//import Foundation
//
//// MARK: - Welcome
//struct userNotificationModel: Codable {
//    var success: Bool
//    var code: Int
//    var message: String
//    var body: [userNotificationModelBody]
//}
//
//// MARK: - Body
//struct userNotificationModelBody: Codable {
//    var id, userID, businessID: Int
//    var businessName, businessImage, message: String
//    var status, type: Int
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case userID = "user_id"
//        case businessID = "business_id"
//        case businessName = "business_name"
//        case businessImage = "business_image"
//        case message, status, type
//    }
//}


import Foundation

// MARK: - Welcome
struct userNotificationModel: Codable {
    let success: Bool
    let code: Int
    let message: String
    let body: [userNotificationModelBody]
}

// MARK: - Body
struct userNotificationModelBody: Codable {
    let id, userID, businessID: Int
    let businessName, businessImage, message: String
    let status, type: Int

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case businessID = "business_id"
        case businessName = "business_name"
        case businessImage = "business_image"
        case message, status, type
    }
}

