// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let businessHomeModel = try? JSONDecoder().decode(BusinessHomeModel.self, from: jsonData)
//
//import Foundation
//
//// MARK: - BusinessHomeModel
//struct BusinessHomeModel: Codable {
//    var success: Bool?
//    var code: Int?
//    var message: String?
//    var body: BusinessHomeModelBody?
//}
//
//// MARK: - Body
//struct BusinessHomeModelBody: Codable {
//    var banners: [Banner]?
//    var businesses: [Business]?
//}
//
//// MARK: - Banner
//struct Banner: Codable {
//    var id: Int?
//    var name, banner: String?
//    var status: Bool?
//    var createdAt, updatedAt: String?
//}
//
//// MARK: - Business
//struct Business: Codable {
//    var id, userID: Int?
//    var name: String?
//    var categoryID, subCategoryID, phone, status: Int?
//    var isApproved: Int?
//    var image, video, location: String?
//    var latitude, longitude: Int?
//    var shortDescription: String?
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case userID = "user_id"
//        case name
//        case categoryID = "category_id"
//        case subCategoryID = "sub_category_id"
//        case phone, status
//        case isApproved = "is_approved"
//        case image, video, location, latitude, longitude
//        case shortDescription = "short_description"
//    }
//}

import Foundation

// MARK: - BuisnessHometModel
struct BusinessHomeModel: Codable {
    let success: Bool?
    let code: Int?
    let message: String?
    let body: BusinessHomeModelBody?
}

// MARK: - Body
struct BusinessHomeModelBody: Codable {
    let banners: [Banner]?
    let businesses: [Business]?
}

// MARK: - Banner
struct Banner: Codable {
    let id: Int?
    let name, banner: String?
    let status: Bool?
    let createdAt, updatedAt: String?
}

// MARK: - Business
struct Business: Codable {
    let id, userID: Int?
    let name: String?
    let categoryID, subCategoryID, phone, status: Int?
    let favouriteUnfavourite, isApproved: Int?
    let image, video, location, latitude: String?
    let longitude, shortDescription: String?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case name
        case categoryID = "category_id"
        case subCategoryID = "sub_category_id"
        case phone, status
        case favouriteUnfavourite = "favourite_unfavourite"
        case isApproved = "is_approved"
        case image, video, location, latitude, longitude
        case shortDescription = "short_description"
    }
}
