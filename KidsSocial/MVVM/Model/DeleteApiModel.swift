//
//  DeleteApiModel.swift
//  KidsSocial
//
//  Created by cqlpc on 19/09/23.
//

import Foundation

// MARK: - DeleteAccount
struct DeleteAccount: Codable {
    let success: Bool
    let code: Int
    let message: String
    let body: DeleteAccountBody
}

// MARK: - Body
struct DeleteAccountBody: Codable {
}
