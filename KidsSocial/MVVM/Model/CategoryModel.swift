//
//  CategoryModel.swift
//  KidsSocial
//
//  Created by cql131 on 15/06/23.
//

import Foundation

// MARK: - CategoryModel
struct CategoryModel: Codable {
    var success: Bool?
    var code: Int?
    var message: String?
    var body: [CategoryModelBody]?
}

// MARK: - Body
struct CategoryModelBody: Codable {
    var id: Int?
    var name, image: String?
    var status: Int?
    var createdAt, updatedAt: String?
}
