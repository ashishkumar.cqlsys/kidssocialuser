//
//  UserHomeModel.swift
//  KidsSocial
//
//  Created by cql131 on 21/06/23.
//
//
//import Foundation
//
//// MARK: - UserHomeModel
//struct UserHomeModel: Codable {
//    var success: Bool?
//    var code: Int?
//    var message: String?
//    var body: UserHomeModelBody?
//}
//
//// MARK: - Body
//struct UserHomeModelBody: Codable {
//    var banners, categories: [BannerUser]?
//    var businesses: [BusinessUser]?
//}
//
//// MARK: - Banner
//struct BannerUser: Codable {
//    var id: Int?
//    var name, banner: String?
//    var status: Bool?
//    var createdAt, updatedAt, image: String?
//}
//
//// MARK: - Business
//struct BusinessUser: Codable {
//    var id, userID: Int?
//    var name: String?
//    var categoryID, subCategoryID, phone, status: Int?
//    var isApproved: Int?
//    var image, video, location, latitude: String?
//    var longitude, shortDescription: String?
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case userID = "user_id"
//        case name
//        case categoryID = "category_id"
//        case subCategoryID = "sub_category_id"
//        case phone, status
//        case isApproved = "is_approved"
//        case image, video, location, latitude, longitude
//        case shortDescription = "short_description"
//    }
//}

import Foundation

// MARK: - UserHomeModel
struct UserHomeModel: Codable {
    var success: Bool?
    var code: Int?
    var message: String?
    var body: UserHomeModelBody?
}

// MARK: - Body
struct UserHomeModelBody: Codable {
    var banners, categories: [BannerUser]?
    var businesses: [BusinessUser]?
}

// MARK: - Banner
struct BannerUser: Codable {
    var id: Int?
    var name, banner: String?
    var status: Bool?
    var createdAt, updatedAt, image: String?
}

// MARK: - Business
struct BusinessUser: Codable {
    var id, userID: Int?
    var name: String?
    var categoryID, subCategoryID, phone, status: Int?
    var favouriteUnfavourite, isfavourite ,isApproved: Int?
    var image, video, location, latitude: String?
    var longitude, shortDescription: String?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case name
        case categoryID = "category_id"
        case subCategoryID = "sub_category_id"
        case phone, status
        case isfavourite = "is_favourite"
        case favouriteUnfavourite = "favourite_unfavourite"
        case isApproved = "is_approved"
        case image, video, location, latitude, longitude
        case shortDescription = "short_description"
    }
}
