//
//  AddBuisnessModel.swift
//  KidsSocial
//
//  Created by cql131 on 15/06/23.
//
//
//import Foundation
//
//// MARK: - AddBuisnessModel
//struct AddBuisnessModel: Codable {
//    var success: Bool?
//    var code: Int?
//    var message: String?
//    var body: AddBuisnessModelBody?
//}
//
//// MARK: - Body
//struct AddBuisnessModelBody: Codable {
//    var id: Int?
//    var name: String?
//    var userID: Int?
//    var phone, shortDescription, categoryID, subCategoryID: String?
//    var location, latitude, longitude, image: String?
//    var video: String?
//
//    enum CodingKeys: String, CodingKey {
//        case id, name
//        case userID = "user_id"
//        case phone
//        case shortDescription = "short_description"
//        case categoryID = "category_id"
//        case subCategoryID = "sub_category_id"
//        case location, latitude, longitude, image, video
//    }
//}

import Foundation

//// MARK: - AddBuisnessModel
//struct AddBuisnessModel: Codable {
//    var success: Bool?
//    var code: Int?
//    var message: String?
//    var body: AddBuisnessModelBody?
//}
//
//// MARK: - Body
//struct AddBuisnessModelBody: Codable {
//    var id, userID: Int?
//    var name: String?
//    var categoryID, subCategoryID, phone, status: Int?
//    var favouriteUnfavourite, isApproved: Int?
//    var image, video, latitude, location: String?
//    var longitude, shortDescription, createdAt, updatedAt: String?
//    var categoryName, subcategoryName: String?
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case userID = "user_id"
//        case name
//        case categoryID = "category_id"
//        case subCategoryID = "sub_category_id"
//        case phone, status
//        case favouriteUnfavourite = "favourite_unfavourite"
//        case isApproved = "is_approved"
//        case image, video, latitude, location, longitude
//        case shortDescription = "short_description"
//        case createdAt = "created_at"
//        case updatedAt = "updated_at"
//        case categoryName = "category_name"
//        case subcategoryName = "subcategory_name"
//    }
//}
import Foundation

// MARK: - AddBuisnessModel
struct AddBuisnessModel: Codable {
    var success: Bool?
    var code: Int?
    var message: String?
    var body: AddBuisnessModelBody?
}

// MARK: - AddBuisnessModelBody
struct AddBuisnessModelBody: Codable {
    var status, favouriteUnfavourite, isApproved, id: Int?
    var name: String?
    var userID: Int?
    var phone, shortDescription, categoryID, subCategoryID: String?
    var location, latitude, longitude, image: String?
    var video: String?

    enum CodingKeys: String, CodingKey {
        case status
        case favouriteUnfavourite = "favourite_unfavourite"
        case isApproved = "is_approved"
        case id, name
        case userID = "user_id"
        case phone
        case shortDescription = "short_description"
        case categoryID = "category_id"
        case subCategoryID = "sub_category_id"
        case location, latitude, longitude, image, video
    }
}
