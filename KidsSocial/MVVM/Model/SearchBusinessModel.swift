//
//  SearchBusinessModel.swift
//  KidsSocial
//
//  Created by cql131 on 23/06/23.
//


import Foundation

// MARK: - SearchBusinessModel
struct SearchBusinessModel: Codable {
    var success: Bool?
    var code: Int?
    var message: String?
    var body: [SearchBusinessModelBody]?
}

// MARK: - Body
struct SearchBusinessModelBody: Codable {
    var id, userID: Int?
    var name: String?
    var categoryID, subCategoryID, phone, status: Int?
    var favouriteUnfavourite, isApproved: Int?
    var image, video, location, latitude: String?
    var longitude, shortDescription: String?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case name
        case categoryID = "category_id"
        case subCategoryID = "sub_category_id"
        case phone, status
        case favouriteUnfavourite = "favourite_unfavourite"
        case isApproved = "is_approved"
        case image, video, location, latitude, longitude
        case shortDescription = "short_description"
    }
}
