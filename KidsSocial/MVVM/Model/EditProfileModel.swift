//
//  EditProfileModel.swift
//  KidsSocial
//
//  Created by cql131 on 14/06/23.
//

import Foundation

// MARK: - VerifyOtp
struct editprofilemodel: Codable {
    var success: Bool?
    var code: Int?
    var message: String?
    var body: edirprofilebody?
}

// MARK: - Body
struct edirprofilebody: Codable {
    var id: Int?
    var name, email, phone, countryCode: String?
    var role: Int?
    var image: String?
    var password: String?
    var status: Bool?
    var location, longitude, latitude, profileImg: String?
    var otp, otpVerified: Int?
    var deviceType, deviceToken, socialID ,createdAt: String?
    var updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id, name, email, phone
        case countryCode = "country_code"
        case role, password, status, location, longitude, latitude
        case profileImg = "profile_img"
        case otp
        case otpVerified = "otp_verified"
        case deviceType = "device_type"
        case deviceToken = "device_token"
        case socialID = "social_id"
        case createdAt, updatedAt
    }
}
