//
//  FavListingModel.swift
//  KidsSocial
//
//  Created by cql197 on 26/06/23.
//
import Foundation

// MARK: - FavListingtModel
struct FavListingtModel: Codable {
    let success: Bool?
    let code: Int?
    let message: String?
    let body: [FavListingtModelBody]?
}

// MARK: - Body
struct FavListingtModelBody: Codable {
    let id, userID: Int?
    let name: String?
    let categoryID, subCategoryID, phone, status: Int?
    let favouriteUnfavourite, isApproved: Int?
    let image, video, location, latitude: String?
    let longitude, shortDescription: String?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case name
        case categoryID = "category_id"
        case subCategoryID = "sub_category_id"
        case phone, status
        case favouriteUnfavourite = "favourite_unfavourite"
        case isApproved = "is_approved"
        case image, video, location, latitude, longitude
        case shortDescription = "short_description"
    }
}



