//
//  VerifyOtp.swift
//  KidsSocial
//
//  Created by cql131 on 14/06/23.
//

import Foundation

// MARK: - VerifyOtp
struct VerifyOtpModel: Codable {
    var success: Bool?
    var code: Int?
    var message: String?
    var body: VerifyOtpBody?
}

// MARK: - Body
struct VerifyOtpBody: Codable {
}

