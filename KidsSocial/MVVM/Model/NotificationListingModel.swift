//
//  NotificationListingModel.swift
//  KidsSocial
//
//  Created by cql131 on 29/06/23.
//
//import Foundation
//
//
//struct NotificationListingModel: Codable {
//    var success: Bool
//    var code: Int
//    var message: String
//    var body: [NotificationListingModelBody]
//}
//
//// MARK: - NotificationListingModelBody
//struct NotificationListingModelBody: Codable {
//    var id, userID, businessID: Int
//    var message: String
//    var status, type: Int
//    var createdAt, updatedAt: String
//    var business: BusinessNotification
//    var user: User
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case userID = "user_id"
//        case businessID = "business_id"
//        case message, status, type
//        case createdAt = "created_at"
//        case updatedAt = "updated_at"
//        case business, user
//    }
//}
//
//// MARK: - BusinessNotification
//struct BusinessNotification: Codable {
//    var id, userID: Int
//    var name: String
//    var categoryID, subCategoryID, phone, status: Int
//    var favouriteUnfavourite, isApproved: Int
//    var image, video, thumbnail, location: String
//    var latitude, longitude, shortDescription: String
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case userID = "user_id"
//        case name
//        case categoryID = "category_id"
//        case subCategoryID = "sub_category_id"
//        case phone, status
//        case favouriteUnfavourite = "favourite_unfavourite"
//        case isApproved = "is_approved"
//        case image, video, thumbnail, location, latitude, longitude
//        case shortDescription = "short_description"
//    }
//}
//
//// MARK: - User
//struct User: Codable {
//    var image, name: String
//}

import Foundation

// MARK: - Welcome
struct NotificationListingModel: Codable {
    var success: Bool
    var code: Int
    var message: String
    var body: [NotificationListingModelBody]
}

// MARK: - Body
struct NotificationListingModelBody: Codable {
    var id, userID, businessID: Int
    var message: String
    var status, type: Int
    var createdAt, updatedAt: String
    var user: User

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case businessID = "business_id"
        case message, status, type
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case user
    }
}

// MARK: - User
struct User: Codable {
    var image, name: String
}
