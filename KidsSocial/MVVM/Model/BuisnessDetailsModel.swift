//
//  BuisnessDetailsModel.swift
//  KidsSocial
//
//  Created by cql131 on 21/06/23.
//


//import Foundation
//
//// MARK: - BusinesssDetailsModel
//struct BusinessDetailsModel: Codable {
//    var success: Bool?
//    var code: Int?
//    var message: String?
//    var body: BusinessDetailsModelBody?
//}

//// MARK: - Body
//struct BusinessDetailsModelBody: Codable {
//    var original: Original?
//    var similar: [Original]?
//}
//
//// MARK: - Original
//struct Original: Codable {
//    var id, userID: Int?
//    var name: String?
//    var categoryID, subCategoryID, phone, status: Int?
//    var favouriteUnfavourite, isApproved: Int?
//    var image, video, latitude, thumbnail: String?
//    var location, longitude, shortDescription, createdAt: String?
//    var updatedAt, categoryName, subcategoryName, username: String?
//    var useremail: String?
//    var isFavourite: Int?
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case userID = "user_id"
//        case name
//        case categoryID = "category_id"
//        case subCategoryID = "sub_category_id"
//        case phone, status
//        case favouriteUnfavourite = "favourite_unfavourite"
//        case isApproved = "is_approved"
//        case image, video, latitude, thumbnail, location, longitude
//        case shortDescription = "short_description"
//        case createdAt = "created_at"
//        case updatedAt = "updated_at"
//        case categoryName = "category_name"
//        case subcategoryName = "subcategory_name"
//        case username, useremail
//        case isFavourite = "is_favourite"
//    }
//}

//import Foundation
//
//// MARK: - SocialLogin
//struct BusinessDetailsModel: Codable {
//    var success: Bool
//    var code: Int
//    var message: String
//    var body: BusinessDetailsModelBody
//}
//
//// MARK: - Body
//struct BusinessDetailsModelBody: Codable {
//    var original: Original
//   // var similar: [Original]
//}
//
//// MARK: - Original
//struct Original: Codable {
//    var id, userID: Int
//    var name: String
//    var categoryID, subCategoryID, phone, status: Int
//    var favouriteUnfavourite, isApproved: Int
//    var image, video, latitude, thumbnail: String
//    var location, longitude, shortDescription, createdAt: String
//    var updatedAt, categoryName: String
//    var subcategoryName = "subcategory_name"
//    var username, email, userImage: String
//    var isFavourite: Int
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case userID = "user_id"
//        case name
//        case categoryID = "category_id"
//        case subCategoryID = "sub_category_id"
//        case phone, status
//        case favouriteUnfavourite = "favourite_unfavourite"
//        case isApproved = "is_approved"
//        case image, video, latitude, thumbnail, location, longitude
//        case shortDescription = "short_description"
//        case createdAt = "created_at"
//        case updatedAt = "updated_at"
//        case categoryName = "category_name"
//        case subcategoryName = "subcategory_name"
//        case username, email
//        case userImage = "user_image"
//        case isFavourite = "is_favourite"
//    }
//}


//import Foundation
//
//// MARK: - SocialLogin
//struct BusinessDetailsModel: Codable {
//    var success: Bool
//    var code: Int
//    var message: String
//    var body: BusinessDetailsModelBody
//}
//
//// MARK: - Body
//struct BusinessDetailsModelBody: Codable {
//    var original: Original
//    var similar: [Similar]
//}
//
//// MARK: - Original
//struct Original: Codable {
//    var id, userID: Int
//    var name: String
//    var categoryID, subCategoryID, phone, status: Int
//    var favouriteUnfavourite, isApproved: Int
//    var image, video, latitude, thumbnail: String
//    var location, longitude, shortDescription, createdAt: String
//    var updatedAt, categoryName, subcategoryName, username: String
//    var email, userImage: String
//    var isFavourite: Int
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case userID = "user_id"
//        case name
//        case categoryID = "category_id"
//        case subCategoryID = "sub_category_id"
//        case phone, status
//        case favouriteUnfavourite = "favourite_unfavourite"
//        case isApproved = "is_approved"
//        case image, video, latitude, thumbnail, location, longitude
//        case shortDescription = "short_description"
//        case createdAt = "created_at"
//        case updatedAt = "updated_at"
//        case categoryName = "category_name"
//        case subcategoryName = "subcategory_name"
//        case username, email
//        case userImage = "user_image"
//        case isFavourite = "is_favourite"
//    }
//}
//
//// MARK: - Similar
//struct Similar: Codable {
//    var id, userID: Int
//    var name: String
//    var categoryID, subCategoryID, phone, status: Int
//    var favouriteUnfavourite, isApproved: Int
//    var image, video, thumbnail, location: String
//    var latitude, longitude, shortDescription: String
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case userID = "user_id"
//        case name
//        case categoryID = "category_id"
//        case subCategoryID = "sub_category_id"
//        case phone, status
//        case favouriteUnfavourite = "favourite_unfavourite"
//        case isApproved = "is_approved"
//        case image, video, thumbnail, location, latitude, longitude
//        case shortDescription = "short_description"
//    }
//}


import Foundation

// MARK: - BusinessDetailsModel
struct BusinessDetailsModel: Codable {
    var success: Bool
    var code: Int
    var message: String
    var body: BusinessDetailsModelBody
}

// MARK: - Body
struct BusinessDetailsModelBody: Codable {
    var original: Original
    var similar: [Original]
}

// MARK: - Original
struct Original: Codable {
    var id, userID: Int
    var name: String
    var categoryID, subCategoryID, phone, status: Int
    var favouriteUnfavourite, isApproved: Int
    var image, video, latitude, thumbnail: String
    var location, longitude, shortDescription, createdAt: String
    var updatedAt: String
    var categoryName, subcategoryName, username, email: String?
    var userImage: String?
    var isFavourite: Int?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case name
        case categoryID = "category_id"
        case subCategoryID = "sub_category_id"
        case phone, status
        case favouriteUnfavourite = "favourite_unfavourite"
        case isApproved = "is_approved"
        case image, video, latitude, thumbnail, location, longitude
        case shortDescription = "short_description"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case categoryName = "category_name"
        case subcategoryName = "subcategory_name"
        case username, email
        case userImage = "user_image"
        case isFavourite = "is_favourite"
    }
}
