//
//  ResendOTPModel.swift
//  KidsSocial
//
//  Created by cql131 on 15/06/23.
//

import Foundation

// MARK: - ResendOTPModel
struct ResendOTPModel: Codable {
    var success: Bool?
    var code: Int?
    var message: String?
    var body: ResendOTPBody?
}

// MARK: - Body
struct ResendOTPBody: Codable {
    var id: Int?
    var name, email, phone, countryCode: String?
    var image: String?
    var role, otpVerified: Int?
    var password: String?
    var status: Bool?
    var location, longitude, latitude, profileImg: String?
    var otp: Int?
    var deviceType, deviceToken, socialID, createdAt: String?
    var updatedAt: String?

    enum CodingKeys: String, CodingKey {
        case id, name, email, phone
        case countryCode = "country_code"
        case image, role
        case otpVerified = "otp_verified"
        case password, status, location, longitude, latitude
        case profileImg = "profile_img"
        case otp
        case deviceType = "device_type"
        case deviceToken = "device_token"
        case socialID = "social_id"
        case createdAt, updatedAt
    }
}
