//
//  EditBusinessModel.swift
//  KidsSocial
//
//  Created by cql131 on 27/06/23.
//


import Foundation

// MARK: - EditbusinessModel
struct EditbusinessModel: Codable {
    var success: Bool?
    var code: Int?
    var message: String?
    var body: EditbusinessModelbody?
}

// MARK: - Body
struct EditbusinessModelbody: Codable {
    var id, userID: Int?
    var name: String?
    var categoryID, subCategoryID, phone, status: Int?
    var favouriteUnfavourite, business_id,isApproved: Int?
    var image, video, location, latitude: String?
    var longitude, shortDescription: String?

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case name
        case business_id
        case categoryID = "category_id"
        case subCategoryID = "sub_category_id"
        case phone, status
        case favouriteUnfavourite = "favourite_unfavourite"
        case isApproved = "is_approved"
        case image, video, location, latitude, longitude
        case shortDescription = "short_description"
    }
}
