//
//  FavoriteModel.swift
//  KidsSocial
//
//  Created by cql131 on 30/06/23.
//
import Foundation

// MARK: - FavoriteModel
struct FavoriteModel: Codable {
    var success: Bool?
    var code: Int?
    var message: String?
    var body: FavoriteModelBody?
}

// MARK: - Body
struct FavoriteModelBody: Codable {
    var status: Int?
}
