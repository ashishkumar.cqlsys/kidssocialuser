//
//  AboutusModel.swift
//  KidsSocial
//
//  Created by cql131 on 14/06/23.
//

import Foundation

// MARK: - AboutusModel
struct AboutusModel: Codable {
    var success: Bool?
    var code: Int?
    var message: String?
    var body: AboutusModelbody?
}

// MARK: - Body
struct AboutusModelbody: Codable {
    var accessor, value: String?
}
