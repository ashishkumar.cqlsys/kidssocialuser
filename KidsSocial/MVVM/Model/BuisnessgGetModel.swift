//
//  BuisnessgGetModel.swift
//  KidsSocial
//
//  Created by cql197 on 26/06/23.
//
//import Foundation
//
//// MARK: - BuisnessGetModel
//struct BuisnessGetModel: Codable {
//    var success: Bool?
//    var code: Int?
//    var message: String?
//    var body: [BuisnessGetModelBody]?
//}
//
//// MARK: - Body
//struct BuisnessGetModelBody: Codable {
//    var id, userID: Int?
//    var name: String?
//    var categoryID, subCategoryID, phone, status: Int?
//    var favouriteUnfavourite, is_favourite ,isApproved: Int?
//    var image, video, location, latitude: String?
//    var longitude, shortDescription: String?
//
//    enum CodingKeys: String, CodingKey {
//        case id
//        case userID = "user_id"
//        case name
//        case is_favourite
//        case categoryID = "category_id"
//        case subCategoryID = "sub_category_id"
//        case phone, status
//        case favouriteUnfavourite = "favourite_unfavourite"
//        case isApproved = "is_approved"
//        case image, video, location, latitude, longitude
//        case shortDescription = "short_description"
//    }
//}
import Foundation

// MARK: - UserHomeModel
struct BuisnessGetModel: Codable {
    var success: Bool?
    var code: Int?
    var message: String?
    var body: [BuisnessGetModelBody]?
}

// MARK: - Body
struct BuisnessGetModelBody: Codable {
    var id: Int?
    var name: String?
    var categoryID, subCategoryID, phone, status: Int?
    var isApproved: Int?
    var image, video, latitude, location: String?
    var longitude, shortDescription, createdAt, updatedAt: String?
    var isFavourite: Int?

    enum CodingKeys: String, CodingKey {
        case id, name
        case categoryID = "category_id"
        case subCategoryID = "sub_category_id"
        case phone, status
        case isApproved = "is_approved"
        case image, video, latitude, location, longitude
        case shortDescription = "short_description"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isFavourite = "is_favourite"
    }
}
