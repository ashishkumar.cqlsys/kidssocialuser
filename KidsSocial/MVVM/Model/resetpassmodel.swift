//
//  resetpassmodel.swift
//  KidsSocial
//
//  Created by cqlpc on 07/07/23.
//


import Foundation

// MARK: - Welcome
struct resetpasswordModel: Codable {
    var success: Bool
    var code: Int
    var message: String
    var body: resetpasswordModelBody
}

// MARK: - Body
struct resetpasswordModelBody: Codable {
}
