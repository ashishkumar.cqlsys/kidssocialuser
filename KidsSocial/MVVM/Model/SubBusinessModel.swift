//
//  SubBusinessModel.swift
//  KidsSocial
//
//  Created by cqlpc on 07/07/23.
//

import Foundation

// MARK: - Welcome
struct SubBusinessModel: Codable {
    var success: Bool
    var code: Int
    var message: String
    var body: [SubBusinessModelBody]
}

// MARK: - Body
struct SubBusinessModelBody: Codable {
    var id, userID: Int
    var name: String
    var categoryID, subCategoryID, phone, status: Int
    var favouriteUnfavourite, isApproved: Int
    var image, video, thumbnail, location: String
    var latitude, longitude, shortDescription: String

    enum CodingKeys: String, CodingKey {
        case id
        case userID = "user_id"
        case name
        case categoryID = "category_id"
        case subCategoryID = "sub_category_id"
        case phone, status
        case favouriteUnfavourite = "favourite_unfavourite"
        case isApproved = "is_approved"
        case image, video, thumbnail, location, latitude, longitude
        case shortDescription = "short_description"
    }
}
