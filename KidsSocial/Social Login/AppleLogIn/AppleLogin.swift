


import Foundation
import AuthenticationServices
import LocalAuthentication

@available(iOS 13.0, *)

class AppleLogin:NSObject {
   
   
    private override init() {}
    
    typealias COMPLETION_HANDLER = (AppleModel)->Void
    var completionHandler: COMPLETION_HANDLER?
    
    func setup(_ completion: @escaping COMPLETION_HANDLER) {
        self.completionHandler = completion
        self.handleAppleIdRequest()
    }
  
    static let Shared:AppleLogin = {
        let Shared = AppleLogin()
        return Shared
    }()
    
    
    
   
    
  
    
}
extension AppleLogin: ASAuthorizationControllerDelegate ,ASAuthorizationControllerPresentationContextProviding{
    @available(iOS 13.0, *)
    
    @objc func handleAppleIdRequest() {
        if #available(iOS 13.0, *) {
            let appleIDProvider = ASAuthorizationAppleIDProvider()
            let request = appleIDProvider.createRequest()
            request.requestedScopes = [.fullName, .email]
            let authorizationController = ASAuthorizationController(authorizationRequests: [request])
            authorizationController.delegate = self
            authorizationController.presentationContextProvider = self
            authorizationController.performRequests()
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    
    
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        
     if let appleIDCredential = authorization.credential as?  ASAuthorizationAppleIDCredential {
            let model = AppleModel()
            model.handleData(appleCredential: appleIDCredential)
            if self.completionHandler != nil {
                self.completionHandler!(model)
            }
    }
   }

  
    @available(iOS 13.0, *)
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        print(error.localizedDescription)
      }
    @available(iOS 13.0, *)
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return UIApplication.shared.windows.first!
        
    }
}
class AppleModel{
    var token, name, firstName, lastName, id, email, picture: String!
    @available(iOS 13.0, *)
    
     func handleData(appleCredential: ASAuthorizationAppleIDCredential) {
            if let identityTokenData = appleCredential.identityToken,
                let identityTokenString = String(data: identityTokenData, encoding: .utf8) {
                self.token = identityTokenString
                
                if let currentUserIdentifier: String = KeychainItem.currentUserIdentifier{
                    if currentUserIdentifier == appleCredential.user {
                        self.id = currentUserIdentifier
                    }else{
                        self.id = appleCredential.user
                    }
                }else{
                    KeychainItem.currentUserIdentifier = appleCredential.user
                    self.id = appleCredential.user
                }
                
                
                if appleCredential.fullName?.familyName == nil{
                    self.lastName = KeychainItem.currentUserLastName
                }else{
                    KeychainItem.currentUserLastName = appleCredential.fullName?.familyName
                    self.lastName = appleCredential.fullName?.familyName
                    }
                }
                if appleCredential.fullName?.givenName == nil{
                    self.firstName = KeychainItem.currentUserFirstName
                    }else{
                        KeychainItem.currentUserFirstName = appleCredential.fullName?.givenName
                        self.firstName = appleCredential.fullName?.givenName
                    }
                
                if appleCredential.email == nil{
                    self.email = KeychainItem.currentUserEmail
                    }else{
                        KeychainItem.currentUserEmail = appleCredential.email
                    self.email = appleCredential.email
                    }
                self.name = "\(self.firstName ?? "") \(self.lastName ?? "")"
            }
        
}
