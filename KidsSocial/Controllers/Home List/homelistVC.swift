//
//  homelistVC.swift
//  KidsSocial
//
//  Created by cqlpc on 05/07/23.
//

import UIKit
import CoreLocation
import SwiftGifOrigin

class homelistVC: UIViewController {
    
    @IBOutlet weak var imgGif: UIImageView!
    @IBOutlet weak var tblVW: UITableView!
    
    var viewmodel = UserViewModel()
    var homeData: UserHomeModelBody?
    var longitude = Double()
    var latitude = Double()
    let locationManager = CLLocationManager()
//    var datagetApi = [FavListingtModelBody]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //setupApi()
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        locationManager.requestWhenInUseAuthorization()
        // Do any additional setup after loading the view.
    }
    func setupApi(){
        viewmodel.userHomeApicall(latitude: self.latitude, longitude: self.longitude) { data in
            self.homeData = data
            self.tblVW.reloadData()
            
        }
    }
        @IBAction func btnBack(_ sender: Any) {
            self.pop()
        }
        
   
}
    //MARK: - EXTENSION
    extension homelistVC : UITableViewDelegate, UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if homeData?.businesses?.count == 0{
                imgGif.image = UIImage.gif(name: "Nodatafound")
                imgGif.isHidden = false
                    }else{
                        imgGif.isHidden = true
                        tblVW.backgroundView = nil
                       
                    }
            return homeData?.businesses?.count ?? 0
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "homelistTVC") as! homelistTVC
            let data = homeData?.businesses?[indexPath.item]
            cell.lblName.text = data?.name
            cell.lblLocation.text = data?.location
            cell.img.setUserImage(URL(string: imageURL + (data?.image ?? "")))
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            let vc = Storyboards.Other.instantiateViewController(withIdentifier: "BussinessDetailVC") as! BussinessDetailVC
            vc.businessid  = homeData?.businesses?[indexPath.row].id ?? 0
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
extension homelistVC : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        self.latitude = locValue.latitude
        self.longitude = locValue.longitude
        setupApi()
        locationManager.stopUpdatingLocation()
    }
}


