//
//  TabBarVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 11/05/23.
//

import UIKit

class TabBarVC: UITabBarController {

//MARK: - viewdidoad
    override func viewDidLoad() {
        super.viewDidLoad()
    }

//MARK: - viewwillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setControllers()
    }

//MARK: - firstUserTab
    func firstUserTab()->UIViewController{
        let vc = Storyboards.Other.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
        vc.tabBarItem = getItem(active: "homeActive", inactive: "homeInactive")
        return vc
    }

//MARK: - secondUserTab
    func secondUserTab()->UIViewController{

            let vc = Storyboards.Other.instantiateViewController(withIdentifier: "FavouriteVC") as! FavouriteVC
            vc.tabBarItem = getItem(active: "likeActive", inactive: "likeInactive")
            return vc

    }

//MARK: - thirdUserTab
    func thirdUserTab()->UIViewController{
        let vc = Storyboards.Other.instantiateViewController(withIdentifier: "SearchVC") as! SearchVC
        vc.tabBarItem = getItem(active: "searchActive", inactive: "searchInactive")
        return vc
    }

//MARK: - fourthUserTab

    func fourthUserTab()->UIViewController{
        if Store.userDetails == nil {
            if UserDefaults.standard.bool(forKey: "ok") == false{
                let vc = Storyboards.profile.instantiateViewController(withIdentifier: "SkipProfileVC") as! SkipProfileVC
                vc.tabBarItem = getItem(active: "profileActive", inactive: "profileInactive")
                return vc
            }
        }else {
            let vc = Storyboards.profile.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            vc.tabBarItem = getItem(active: "profileActive", inactive: "profileInactive")
            return vc
        }
        return UIViewController()
    }

//MARK: - setControllers
    
    func setControllers(){
        self.viewControllers = [firstUserTab(),secondUserTab(),thirdUserTab(),fourthUserTab()]
    }

//MARK: - get tab bar item
    func getItem(title:String = "",active:String,inactive:String)-> UITabBarItem{
        let icon = UITabBarItem(title: title, image: UIImage(named: inactive), selectedImage: UIImage(named: active))
        return icon
    }
}
