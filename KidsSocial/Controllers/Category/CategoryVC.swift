//
//  CategoryVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 15/05/23.
//

import UIKit
import SDWebImage
import CoreLocation
import SwiftGifOrigin
import CoreMedia

class CategoryVC: UIViewController {
    //MARK: - outlets
    @IBOutlet weak var lbllocation: UILabel!
    @IBOutlet weak var lblacategory: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var searchTxt: UITextField!
    @IBOutlet weak var subCatTable: UITableView!
    @IBOutlet weak var playCollectionView: UICollectionView!
    @IBOutlet weak var nearByBussinessCollVw: UICollectionView!
    //MARK: - variables
    var headerArr = ["Play","View Business"]
    var getcatid = Int()
    var viewmodel = BuisnessviewModel()
    var model = [SubcategoryModelBody]()
    var viemodelSecond = UserViewModel()
    var dataBusiness = [BusinessUser]()
    var filterData = [BusinessUser]()
    var longitude = Double()
    var latitude = Double()
    var viewmodelsecond = BuisnessviewModel()
    let locationManager = CLLocationManager()
    var isstatus = Int()
    var imageView: UIImageView?
    var businessID = Int()
    var statuschange = Int()
    var isLike = Int()
    
    //MARK: - viewdidload
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Categoryclass")
        searchTxt.delegate = self
        searchTxt.text?.isEmpty
        changingData = 1
        self.setupsubcategory {
            self.setupApi()
        }
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        locationManager.requestWhenInUseAuthorization()
    }
//    MARK: - FUNCTION API
    func setupsubcategory(onsuccess:@escaping(()->())) {
        viewmodel.subcategoryapicall(categoryId: getcatid) { data in
            if data?.count ?? 0 > 0{
                self.model = data ?? []
                self.playCollectionView.reloadData()
                onsuccess()
            }
        }
    }
//        MARK: - FUNCTION API
    func setupApi(){
        viemodelSecond.userHomeApicall(latitude: self.latitude, longitude: self.longitude) { data in
            self.dataBusiness = data?.businesses ?? []
           
            self.filterData = data?.businesses ?? []
        //    self.lbllocation.text = data?.businesses?.first?.location
            self.nearByBussinessCollVw.reloadData()
            
        }
    }
    //MARK: - uiload
    override func loadView() {
        super.loadView()
//        subCatTable.register(UINib(nibName: "HomeTableCell", bundle: nil), forCellReuseIdentifier: "HomeTableCell")
    }
    //MARK: - action
    @IBAction func backTapped(_ sender: Any) {
        pop()
    }
    
}

//MARK: -
extension CategoryVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == playCollectionView{
            return model.count ?? 0
        }
        else {
            let count = filterData.count
                   
                   if count == 0 {
                       if imageView == nil {
                           imageView = createCenteredImageView(image: UIImage.gif(name: "nodataFound"), width: 300, height: 300)
                       }
                   } else {
                       imageView?.removeFromSuperview()
                       imageView = nil
                   }
                    return count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == playCollectionView {
            let data = model[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCVCone", for: indexPath) as! SubCategoryCVCone
            cell.lblSubCategory.text = data.subcatName ?? ""
            cell.subCategoryImage.setUserImage(URL(string: categoryURl + (data.image ?? "")))
//            cell.subCategoryImage.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NearByCVC", for: indexPath) as! NearByCVC
            
            cell.callbacksecond = {
                let vc = Storyboards.Other.instantiateViewController(withIdentifier: "BussinessDetailVC") as! BussinessDetailVC
                vc.businessid = self.dataBusiness[indexPath.row].id ?? 0
                self.navigationController?.pushViewController(vc, animated: true)
            }
            cell.btnLike.tag = indexPath.row
            cell.btnLike.addTarget(self, action: #selector(likeDeslike), for: .touchUpInside)
            
            
            
            if self.dataBusiness[indexPath.row].isfavourite == 1{
                cell.btnLike.setImage(UIImage.init(named: "like"), for: .normal)
                
            }else{
                cell.btnLike.setImage(UIImage.init(named: "dislike"), for: .normal)
            }
         
            let data = filterData[indexPath.row]
            cell.businessname.text = data.name
            cell.lblLocation.text = data.location
            cell.BusinessImage.setUserImage(URL(string: imageURL + (data.image ?? "")))
//            cell.BusinessImage.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            
            return cell
        }
    }
    @objc func likeDeslike(_ sender:UIButton){
        if UserDefaults.standard.bool(forKey: "ok") == false{
            let alert = UIAlertController(title: "Login", message: "Please login first", preferredStyle: .alert)
                        let ok = UIAlertAction(title: "Login", style: .default, handler: { action in
                                let vc = Storyboards.main.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                self.pushToVC(vc: vc)
                            
                            
                             })
                             alert.addAction(ok)
                        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in self.dismiss(animated: true, completion: nil)
                             })
                             alert.addAction(cancel)
                             DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true)
                        })
        
        }else{
            
            let likeType = dataBusiness[sender.tag].isfavourite ?? 0 == 1 ? 2 : 1
            print("type is======",likeType,dataBusiness[sender.tag].name ?? "")
            
            self.viewmodelsecond.favUnfav(status: likeType, business_id: self.dataBusiness[sender.tag].id ?? 0) {
                self.dataBusiness[sender.tag].isfavourite = likeType == 1 ? 1 : 2
                print("New type is======",self.dataBusiness[sender.tag].isfavourite)
                self.nearByBussinessCollVw.reloadData()
            }
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == playCollectionView{
            return CGSize(width: playCollectionView.frame.width / 4 - 3 , height: playCollectionView.frame.height  )
        }
        else {
//            return CGSize(width: nearByBussinessCollVw.frame.width / 2 , height: nearByBussinessCollVw.frame.height / 2 - 4)
            let width = collectionView.frame.width
            return CGSize(width: width / 2 - 10, height: 260)
        }
            
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == playCollectionView {
            let vc = Storyboards.main.instantiateViewController(withIdentifier: "SubBusinessVC") as! SubBusinessVC
//            print(dataBusiness[indexPath.row].subCategoryID ?? 0)
//            print(dataBusiness[indexPath.row].categoryID ?? 0)
            vc.arrsubcategory = model[indexPath.row].id ?? 0
            vc.arrcategoryId = self.getcatid
            self.pushToVC(vc: vc)
        }
       
  }
    
}
extension CategoryVC : CLLocationManagerDelegate{
  func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
      if status == .authorizedWhenInUse {
          locationManager.startUpdatingLocation()
      }
  }
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
      guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
      print("locations = \(locValue.latitude) \(locValue.longitude)")
      self.latitude = locValue.latitude
      self.longitude = locValue.longitude
      self.setupApi()
      locationManager.stopUpdatingLocation()
      
  }
}
//
////MARK: - EXTENSIONS FOR SEARCH BAR

extension CategoryVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        //        guard let searchStr = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) else {
        //                            return true
        //                        }
        
        if let text = textField.text,
           let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange,
                                                       with: string)
            self.lbllocation.text = updatedText
            
            if updatedText == "" {
                self.filterData = self.dataBusiness
            } else {
                self.filterData = dataBusiness.filter{($0.location ?? "").lowercased()
                    .contains(text.lowercased())}
            }
            self.nearByBussinessCollVw.reloadData()
        
        }
        
        return true
    }
}

