//
//  UserBusinessPopUp.swift
//  KidsSocial
//
//  Created by cqlpc on 14/07/23.
//

import UIKit

class UserBusinessPopUp: UIViewController {
    @IBOutlet weak var btnBusiness: PinkBorderBtn!
    @IBOutlet weak var btnuser: ThemeButton!
    var role = Int()
    var callBack:((Int)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    func setup(){
        self.role = 1
        isBusiness = false
        btnuser.pinkThemebtn()
        btnBusiness.pinkBorderBtn()
    }
    
    @IBAction func btnUser(_ sender: UIButton) {
        self.role = 1
        btnuser.pinkThemebtn()
        btnuser.pinkBorderBtn()
        self.dismiss(animated: true) {
            self.callBack?(1)
        }
    }
    
    @IBAction func btnBusiness(_ sender: Any) {
        self.role = 2
        btnuser.pinkBorderBtn()
        btnBusiness.pinkThemebtn()
        self.dismiss(animated: true) {
            self.callBack?(2)
        }
        
    }
    @IBAction func btnCross(_ sender: Any) {
        self.dismiss(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
