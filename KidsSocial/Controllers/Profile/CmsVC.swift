//
//  CmsVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 10/05/23.
//

import UIKit

class CmsVC: UIViewController {
//MARK: - outlets
    @IBOutlet weak var cmsText: UITextView!
    @IBOutlet weak var screenTitle: UILabel!
    
    //MARK: - variable
    var titleStr = ""
    var viewmodel = AuthviewModel()
    //MARK: - viewdidload
    override func viewDidLoad() {
        super.viewDidLoad()
        screenTitle.text = titleStr
        if self.titleStr == "About Us" {
            viewmodel.aboutUsApi() { data in
                self.cmsText.text = data?.value?.htmlToString
        }
        }
        else if self.titleStr == "Privacy Policy" {
            viewmodel.privacypolicyApi { data  in
                self.cmsText.text = data?.value?.htmlToString
            }
        } else{
            viewmodel.termsandconditionApi { data in
                self.cmsText.text = data?.value?.htmlToString
            }
        }
    }
//        setText()
//        screenTitle.text = titleStr
  
    //
//
//    func setText(){
//        let text = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simp dummy text of the printing and typesetting industry.\n\n Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simp dummy text of the printing and typesetting industry.\n\n Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simp dummy text of the printing and typesetting industry.\n\n Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simp dummy text of the printing and typesetting industry.\n\n Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simp dummy text of the printing and typesetting industry.\n\n Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simp dummy text of the printing and typesetting industry."
//        cmsText.text = text
  
    //MARK: - button actions
    @IBAction func backTapped(_ sender: Any) {
        pop()
    }
}
