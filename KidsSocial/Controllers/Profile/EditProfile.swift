//
//  EditProfile.swift
//  KidsSocial
//
//  Created by AjayDhiman on 10/05/23.
//

import UIKit
import SDWebImage
import AVFoundation
class EditProfile: UIViewController {
    
    //    MARK: - OUTLETS
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var mobileNoTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var nameLbl: UILabel!
    
    //    MARK: - VARIABLE
    var viewmodel = AuthviewModel()
    var arrdataget: getProfileModalBody?
    
    //    MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
        
    }
    //MARK: - FUNCTIONS
    private func setup(){
        viewmodel.getprofile { data  in
            self.arrdataget = data
            self.emailTF.text = data?.email
            self.nameTF.text = data?.name
            self.mobileNoTF.text = data?.phone
            self.profileImg.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            self.profileImg.sd_setImage(with: URL(string: imageURL + (data?.image ?? "")),placeholderImage: UIImage(named: "pl"))
            //            self.profileImg.setUserImage(URL(string: imageURL + (data?.image ?? "")))
            
        }
    }
    
    
    //    MARK: - ACTIONS
    @IBAction func backTapped(_ sender: Any) {
        pop()
    }
    @IBAction func updateTapped(_ sender: Any) {
        viewmodel.editprofileapicall(name: nameTF.text ?? "", mobile: mobileNoTF.text ?? "", image: profileImg.image ?? UIImage()) { _ in
            self.pop()
        }
    }
    
    
    @IBAction func cameraTApped(_ sender: Any) {
        self.checkCameraAccess()
        imagePickerClass(false, false, true).showPicker(self) { (image, type, url) in
            self.profileImg.image = image
            
        }
    }

   }
//EXTENSION CAMERA ACCESS
extension EditProfile {
    func checkCameraAccess() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            print("Denied, request permission from settings")
            presentCameraSettings()
        case .restricted:
            print("Restricted, device owner must approve")
        case .authorized:
            print("Authorized, proceed")
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    print("Permission granted, proceed")
                } else {
                    print("Permission denied")
                }
            }
        default: print("kkkkk")
        }
    }
    func presentCameraSettings() {
        let alertController = UIAlertController(title: "Error",
                                                message: "Camera access is denied",
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default))
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    // Handle
                })
            }
        })
        
        present(alertController, animated: true)
    }
}
