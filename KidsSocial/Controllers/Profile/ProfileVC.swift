//
//  ProfileVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 10/05/23.
//

import UIKit
import SDWebImage

class ProfileVC: UIViewController {
    
    //MARK: - OUTLETS
    @IBOutlet weak var profileimage: UIImageView!
    @IBOutlet weak var namelblheader: UILabel!
   @IBOutlet weak var profileView: ShadowView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    
    //MARK: - VARIABELS
    var viewmodal = AuthviewModel()
    var modal : getProfileModalBody?
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    override func viewWillAppear(_ animated: Bool) {
        
//        if Store.userDetails == nil {
//            if UserDefaults.standard.bool(forKey: "ok") == false{
//                let alert = UIAlertController(title: "Login", message: "Please login first", preferredStyle: .alert)
//                let ok = UIAlertAction(title: "Login", style: .default, handler: { action in
//                    let vc = Storyboards.main.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//                    self.pushToVC(vc: vc)
//
//
//                })
//                alert.addAction(ok)
//                let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
//                    //                            self.navigationController?.popViewController(animated: false)
//                    let vc = Storyboards.tab.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
//                    self.navigationController?.pushViewController(vc, animated: false)
//
//                })
//                alert.addAction(cancel)
//                DispatchQueue.main.async(execute: {
//                    self.present(alert, animated: true)
//                })
//
//            }
//        }else{
            setup()
//        }
        
    }
    
    //MARK: - FUNCTIONS
    private func setup(){
        viewmodal.getprofile { data  in
            self.modal = data
            self.lblEmail.text = self.modal?.email ?? ""
            self.lblName.text = self.modal?.name ?? ""
            self.namelblheader.text = self.modal?.name ?? ""
            let imageIndex = (imageURL) + (self.modal?.image ?? "")
            self.profileimage.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            self.profileimage.sd_setImage(with: URL(string: imageIndex),placeholderImage: UIImage(named: "pl"))
//            self.profileimage.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            self.lblMobile.text = self.modal?.phone ?? ""
        }
    }
    
    //MARK: - ACTIONSr
    @IBAction func editTapped(_ sender: UIButton){
        let vc = Storyboards.profile.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
        self.pushToVC(vc: vc)
    }

}
