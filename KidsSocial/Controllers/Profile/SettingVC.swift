//
//  SettingVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 10/05/23.
//

import UIKit

class SettingVC: UIViewController {
//MARK: - outlets
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var settingTable: UITableView!{
        didSet{
            settingTable.tableFooterView = UIView(frame: .zero)
        }
    }
    
//MARK: - variables
    var viewmodel = AuthviewModel()
    var settingArr = [String]()
    //["Notification","Change Password","Privacy Policy","Terms and Conditions","About","Logout"]
    var settingImg = [String]()
    //["notification_ic","change","privacy","terms","about","logOut"]
//MARK: - viewdidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        if Store.userDetails == nil {
            if UserDefaults.standard.bool(forKey: "ok") == false{
                let alert = UIAlertController(title: "Login", message: "Please login first", preferredStyle: .alert)
                let ok = UIAlertAction(title: "Login", style: .default, handler: { action in
                    let vc = Storyboards.main.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.pushToVC(vc: vc)
                    
                    
                })
                alert.addAction(ok)
                let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in self.dismiss(animated: true, completion: nil)
                })
                alert.addAction(cancel)
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true)
                })
                
            }
        }else{}
       
        if fromTabBar{
            backBtn.isHidden = true
        }else{
            backBtn.isHidden = false
        }
        if Store.sociallogin == true {
            settingArr = ["Notification","Privacy Policy","Terms and Conditions","About","Delete Account","Logout"]
            settingImg = ["notification_ic","privacy","terms","about","Group 16189","logOut"]

        }else{
            settingArr = ["Notification","Change Password","Privacy Policy","Terms and Conditions","About","Delete Account","Logout"]
            settingImg = ["notification_ic","change","privacy","terms","about","Group 16189","logOut"]
        }
        
    }
 //MARK: - Action
    @IBAction func backTapped(_ sender: Any) {
        pop()
    }
}
//MARK: - delegate datasource
extension SettingVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
        cell.cellConfig(image: settingImg[indexPath.row], title: settingArr[indexPath.row], index: indexPath.row)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        "Notification","Change Password","Privacy Policy","Terms and Conditions","About","Logout"
        switch settingArr[indexPath.row]{
        case "Notification":
            let vc = Storyboards.profile.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
            self.pushToVC(vc: vc)
        case "Change Password":
            let vc = Storyboards.main.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            self.pushToVC(vc: vc)
        case "Privacy Policy":
            let vc = Storyboards.profile.instantiateViewController(withIdentifier: "CmsVC") as! CmsVC
            vc.titleStr = "Privacy Policy"
            self.pushToVC(vc: vc)
        case "Terms and Conditions":
            let vc = Storyboards.profile.instantiateViewController(withIdentifier: "CmsVC") as! CmsVC
            vc.titleStr = "Terms and Conditions"
            self.pushToVC(vc: vc)
        case "About":
            let vc = Storyboards.profile.instantiateViewController(withIdentifier: "CmsVC") as! CmsVC
            vc.titleStr = "About Us"
            self.pushToVC(vc: vc)
        case "Delete Account":
            let alert = UIAlertController(title: "Delete Account", message: "Are you sure, You want to Delete your account?", preferredStyle: .alert)
                        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                            self.viewmodel.deleteuserApi {
                                Store.autoLogin = false
                                Store.userDetails = nil
                                Store.authKey = nil
                                let vc = Storyboards.main.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                self.pushToVC(vc: vc)

                            }

                            
                             })
                             alert.addAction(ok)
                        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in self.dismiss(animated: true, completion: nil)
                             })
                             alert.addAction(cancel)
                             DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true)
                        })
//            default:
//                print("Not Delete")

        case "Logout":
            let alert = UIAlertController(title: "Logout", message: "Are you sure, You want to logout", preferredStyle: .alert)
                        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
                            
                            self.viewmodel.logoutapicall {
                                Store.autoLogin = false
                                Store.userDetails = nil
                                Store.authKey = nil
                                let vc = Storyboards.main.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                self.pushToVC(vc: vc)
                            }
                            
                             })
                             alert.addAction(ok)
                        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in self.dismiss(animated: true, completion: nil)
                             })
                             alert.addAction(cancel)
                             DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true)
                        })
            default:
                print("hello")
            
            }
          
    }
    
    
}
