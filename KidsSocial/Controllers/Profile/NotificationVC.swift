//
//  NotificationVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 10/05/23.
//

import UIKit
import SDWebImage
class NotificationVC: UIViewController {
    //MARK: - OUTLETS
    @IBOutlet weak var notificationTable: UITableView!
    
    //MARK: -VARIABLE
    var imageView: UIImageView?
    var viewmodel = BuisnessviewModel()
    var datagetAPI : [NotificationListingModelBody]?
    var datagetUsernotification : [userNotificationModelBody]?
    //MARK: - VIEW LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        if Store.userDetails?.body?.role == 1 {
          usernotificationApi()
        }else{
            setupApi()
        }
    }
    //    MARK: - NOTIFICATION API
    func setupApi(){
        viewmodel.notificationListingApiCall { data in
            self.datagetAPI = data?.reversed()
            self.notificationTable.reloadData()
        }
    }
    
    func usernotificationApi(){
        viewmodel.usernotificationListingApiCall { data in
            self.datagetUsernotification  = data?.reversed()
            self.notificationTable.reloadData()
        }
    }
    
    
    //    MARK: - ACTIONS
    @IBAction func backTapped(_ sender: Any) {
        pop()
//        if Store.userDetails?.body?.role == 1 {
//            let vc = Storyboards.tab.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
//                       Store.autoLogin = true
//            self.navigationController?.pushViewController(vc, animated: false)
//        }
//        else {
//            let vc = Storyboards.tab.instantiateViewController(withIdentifier: "BusinessTabBarVC") as! BusinessTabBarVC
//                       Store.autoLogin = true
//            self.navigationController?.pushViewController(vc, animated: false)
//        }
//        pop()
    }
    func showBackgroundGIF() {
               let gifURL = Bundle.main.url(forResource: "Nodatafound", withExtension: "gif") // Replace "noDataGif" with the name of your GIF file
               
               if let gifURL = gifURL, let gifData = try? Data(contentsOf: gifURL), let source = CGImageSourceCreateWithData(gifData as CFData, nil) {
                   let frameCount = CGImageSourceGetCount(source)
                   var images: [UIImage] = []
                   
                   for i in 0..<frameCount {
                       if let cgImage = CGImageSourceCreateImageAtIndex(source, i, nil) {
                           let uiImage = UIImage(cgImage: cgImage)
                           images.append(uiImage)
                       }
                   }
                   
                   imageView = UIImageView(frame: notificationTable.bounds)
                   imageView?.animationImages = images
                   imageView?.animationDuration = TimeInterval(frameCount) * 0.1 // Adjust the animation speed if needed
                   imageView?.animationRepeatCount = 0 // Repeat indefinitely
                   imageView?.contentMode = .scaleAspectFit
                   imageView?.startAnimating()
                   
                   notificationTable.backgroundView = imageView
                   notificationTable.separatorStyle = .none
               }
           }
           
           func removeBackgroundGIF() {
               imageView?.stopAnimating()
               //imageView? = nil
               notificationTable.backgroundView = nil
               notificationTable.separatorStyle = .singleLine
           }
    
}

//MARK: - delegate datasource

extension NotificationVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Store.userDetails?.body?.role == 1 {
            if datagetUsernotification?.count == 0 {
                showBackgroundGIF()
            } else {
                removeBackgroundGIF()
           
            }
            return datagetUsernotification?.count ?? 0
        } else {
            if datagetAPI?.count == 0 {
                showBackgroundGIF()
            } else {
                removeBackgroundGIF()
           
            }
            return datagetAPI?.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTVC") as! NotificationTVC
        let data = datagetAPI?[indexPath.row]
        let datauser = datagetUsernotification?[indexPath.row]
        
        if Store.userDetails?.body?.role == 1 {
            cell.userName.text = datauser?.businessName
            cell.notifiDescription.text = datauser?.message
            cell.userImg.setUserImage(URL(string: imageURL + (datauser?.businessImage ?? "pl")))
            
        }
        else {
            cell.userImg.setUserImage(URL(string: imageURL + (data?.user.image ?? "pl")))
            cell.userName.text = data?.user.name
            cell.notifiDescription.text  = data?.message
        }

        let isoDate =  self.datagetAPI?[indexPath.row].createdAt ?? ""
              let dateFormatter = DateFormatter()
              dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
              let date = dateFormatter.date(from: isoDate ?? "")
         cell.days.text = date?.toLocalTime().timeAgoSinceDate()
   
        return cell
    }
    
    
}

// MARK: - EXTENSION FOR DATE
extension Date {

    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    func timeAgoSinceDate() -> String {

           // From Time
           let fromDate = self

           // To Time
           let toDate = Date()

           // Year
           if let interval = Calendar.current.dateComponents([.year], from: fromDate, to: toDate).year, interval > 0  {

               return interval == 1 ? "\(interval)" + " " + "year ago" : "\(interval)" + " " + "years ago"
           }

           // Month
           if let interval = Calendar.current.dateComponents([.month], from: fromDate, to: toDate).month, interval > 0  {

               return interval == 1 ? "\(interval)" + " " + "mon ago" : "\(interval)" + " " + "mon ago"
           }

           // Day
           if let interval = Calendar.current.dateComponents([.day], from: fromDate, to: toDate).day, interval > 0  {

               return interval == 1 ? "\(interval)" + " " + "day ago" : "\(interval)" + " " + "days ago"
           }

           // Hours
           if let interval = Calendar.current.dateComponents([.hour], from: fromDate, to: toDate).hour, interval > 0 {

               return interval == 1 ? "\(interval)" + " " + "hour ago" : "\(interval)" + " " + "hours ago"
           }

           // Minute
           if let interval = Calendar.current.dateComponents([.minute], from: fromDate, to: toDate).minute, interval > 0 {

               return interval == 1 ? "\(interval)" + " " + "min ago" : "\(interval)" + " " + "min ago"
           }

           return "just now"
       }
   }
