//
//  SkipProfileVC.swift
//  KidsSocial
//
//  Created by cqlpc on 21/09/23.
//

import UIKit

class SkipProfileVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let alert = UIAlertController(title: "Login", message: "Please login first", preferredStyle: .alert)
        let ok = UIAlertAction(title: "Login", style: .default, handler: { action in
            let vc = Storyboards.main.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.pushToVC(vc: vc)
            
            
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
            let vc = Storyboards.tab.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
            self.pushToVC(vc: vc)
        })
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }

}
