//
//  SettingCell.swift
//  KidsSocial
//
//  Created by AjayDhiman on 10/05/23.
//

import UIKit

class SettingCell: UITableViewCell {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    func cellConfig(image: String,title: String,index: Int){
        if index == 6{
            nextBtn.isHidden = true
        }else{
            nextBtn.isHidden = false
        }
        name.text = title
        img.image = UIImage(named: image)
    }

}
