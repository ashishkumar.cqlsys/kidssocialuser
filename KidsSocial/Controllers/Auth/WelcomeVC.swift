//
//  WelcomeVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 10/05/23.
//

import UIKit

class WelcomeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

      
    }
    @IBAction func loginTapped(_ sender: UIButton){
        let vc = Storyboards.main.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.pushToVC(vc: vc)
    }
    @IBAction func createAccTapped(_ sender: UIButton){
        let vc = Storyboards.main.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.pushToVC(vc: vc)
    }

}
