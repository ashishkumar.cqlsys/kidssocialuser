//
//  LoginVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 10/05/23.
//

import UIKit
import GoogleSignIn
import FacebookCore
import FacebookLogin

class LoginVC: UIViewController {

//    MARK: - OUTLETS
    @IBOutlet weak var btnbuisness: PinkBorderBtn!
    @IBOutlet weak var btnuser: ThemeButton!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var btnSkip: UIButton!
    
    //    MARK: - VARIABLE
    var isSeenDelegate = false
    var viewmodel = AuthviewModel()
    var error: NSError?
    let fbLoginManager : LoginManager = LoginManager()
    //    MARK: -  LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().presentingViewController = self
//        self.email.text = "koo@gmail.com"
//        self.password.text = "123456789"
    }
    //    MARK: - ACTIONSr
    @IBAction func backTapped(_ sender: Any) {
        let vc = Storyboards.main.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    @IBAction func forgetTapped(_ sender: Any) {
        let vc = Storyboards.main.instantiateViewController(withIdentifier: "ResetPwdVC") as! ResetPwdVC
        self.pushToVC(vc: vc)
    }
    
    @IBAction func btnSkip(_ sender: UIButton) {
        UserDefaults.standard.set(false, forKey: "ok")
        let vc = Storyboards.tab.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
       
        self.pushToVC(vc: vc)
    }
    @IBAction func loginTapped(_ sender: UIButton){
        UserDefaults.standard.set(true, forKey: "ok")
        print(Store.userDetails?.body?.role)
        if email.text!.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter email", isSuccess: .error)
        }else if email.text!.isValidEmail{
            CommonUtilities.shared.showAlert(message: "Please enter valid email", isSuccess: .error)
        }else if self.password.text!.trimmingCharacters(in: .whitespaces).isEmpty{
            CommonUtilities.shared.showAlert(message: "Please enter password", isSuccess: .error)
        }else if password.text?.count ?? 0 <= 5{
            CommonUtilities.shared.showAlert(message: "Please enter password minimum 6 characters", isSuccess: .error)
        } else{
            let vc = self.storyboard?.instantiateViewController(identifier: "UserBusinessPopUp") as! UserBusinessPopUp
            vc.modalPresentationStyle = .overFullScreen
            vc.callBack = { (role) in
                self.viewmodel.loginApicall(email: self.email.text ?? "", password: self.password.text ?? "",role: role) {_ in
                    if Store.userDetails?.body?.otpVerified == 0 {
                        let vc = Storyboards.main.instantiateViewController(withIdentifier: "OtpVerificationVC") as! OtpVerificationVC
                        self.pushToVC(vc: vc)
                    } else {
                        if Store.userDetails?.body?.role == 1 {
                            isBusiness = false
                            let vc = Storyboards.tab.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                            Store.autoLogin = true
                            self.pushToVC(vc: vc)
                        } else {
                            isBusiness = true
                            let vc = Storyboards.tab.instantiateViewController(withIdentifier: "BusinessTabBarVC") as! BusinessTabBarVC
                            Store.autoLogin = true
                            self.pushToVC(vc: vc)
                        }
                    }
                }
            }
            self.navigationController?.present(vc, animated: true)
        }
    }
    @IBAction func signUpTapped(_ sender: Any) {
            let vc = Storyboards.main.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
            self.pushToVC(vc: vc)
      
       
    }
    @IBAction func userbtn(_ sender: Any) {
        btnuser.pinkThemebtn()
        btnbuisness.pinkBorderBtn()
        
    }
    @IBAction func businessbtn(_ sender: Any) {
        btnuser.pinkBorderBtn()
        btnbuisness.pinkThemebtn()
    }
    
    @IBAction func loginWithGoogle(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func loginWithFB(_ sender: Any) {
    }
    
    @IBAction func loginWithApple(_ sender: Any) {
        self.appleLoginApi()
    }
    
//    MARK: - FUNCTION APPLE LOGIN
    func appleLoginApi(){
        AppleLogin.Shared.setup { (userData) in
            print(userData)
            self.viewmodel.SocialLogin(socialType: "1", socialId: userData.id, name: userData.name, email: userData.email) {
                Store.userDetails?.body?.name = userData.name
                Store.userDetails?.body?.email = userData.email
                Store.userDetails?.body?.otpVerified = 1
                    let vc = Storyboards.tab.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                               Store.autoLogin = true
                Store.sociallogin = true
                               self.pushToVC(vc: vc)

            }
            }
        }
    }
extension LoginVC: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
                debugPrint("The user has not signed in before or they have since signed out.")
            } else {
                debugPrint("\(error.localizedDescription)")
            }
            return
        }
        let userEmail = user.profile?.email ?? ""
        let userName = user.profile?.name ?? ""
        let socialId = user.userID ?? ""
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.viewmodel.SocialLogin(socialType: "1", socialId: socialId, name: userName, email: userEmail) {
                    let vc = Storyboards.tab.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                               Store.autoLogin = true
                                Store.sociallogin = true
                               self.pushToVC(vc: vc)

            }
        }
    }
}

