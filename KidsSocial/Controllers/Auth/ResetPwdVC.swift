//
//  ResetPwdVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 10/05/23.
//

import UIKit

class ResetPwdVC: UIViewController {
    //MARK: - OUTLETS
    @IBOutlet weak var tfEmail: UITextField!
    var viewmodel = AuthviewModel()
    var dataget = resetpasswordModelBody()
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: - ACTIONS
    @IBAction func backtapped(_ sender: Any) {
        pop()
    }
    @IBAction func sendTapped(_ sender: Any) {
        viewmodel.ForgotPassword(email: tfEmail.text ?? "") {
            let vc = Storyboards.main.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            self.pushToVC(vc: vc)
  
        }
           
    }
}
