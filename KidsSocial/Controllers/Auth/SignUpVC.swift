//
//  SignUpVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 10/05/23.
//

import UIKit
import CloudKit

class SignUpVC: UIViewController {
    
    //    MARK: - OUTLET
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var mobile: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var userBtn: UIButton!
    @IBOutlet weak var businessBtn: UIButton!
    
    //    MARK: - VARIABLE
    var viewmodel = AuthviewModel()
    var role = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
      setup()
    }
    
    func setup(){
        self.role = 1
        isBusiness = false
        userBtn.pinkThemebtn()
        businessBtn.pinkBorderBtn()
    }
    
    //MARK: - ACTION
    @IBAction func signInTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func backTapped(_ sender: UIButton){
        self.pop()
    }
    @IBAction func signUpTapped(_ sender: Any) {
        signUpAPICall(MyName: name.text ?? "", myEmail: email.text ?? "", myPhoneNumber: mobile.text ?? "", myPassword: password.text ?? "", myConfirmPassword: confirmPassword.text ?? "", myRole: role)

    }
    
    @IBAction func userTapped(_ sender: Any) {
        self.role = 1
        isBusiness = false
        userBtn.pinkThemebtn()
        businessBtn.pinkBorderBtn()
    }
    
    @IBAction func businessTapped(_ sender: Any) {
        self.role = 2
        isBusiness = true
        userBtn.pinkBorderBtn()
        businessBtn.pinkThemebtn()
    }
}

//MARK: - API'S
extension SignUpVC {
    func signUpAPICall(MyName:String,myEmail:String,myPhoneNumber:String,myPassword:String,myConfirmPassword:String,myRole:Int){
        viewmodel.signupApicall(name:MyName, email: myEmail, phone: myPhoneNumber, password:myPassword, confirmpassword:myConfirmPassword, role: myRole ){ data in
           
            CommonUtilities.shared.showAlert(message: "Otp has been sent to email successfully", isSuccess: .success)
            let vc = Storyboards.main.instantiateViewController(withIdentifier: "OtpVerificationVC") as! OtpVerificationVC
            self.pushToVC(vc: vc)
        }
    }
}
