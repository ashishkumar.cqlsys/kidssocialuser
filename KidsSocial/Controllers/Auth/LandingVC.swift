//
//  LandingVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 09/05/23.
//

import UIKit

class LandingVC: UIViewController{
    
 //MARK: - outlets
    
    @IBOutlet weak var landingCV: UICollectionView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    @IBOutlet weak var pageControl: UIPageControl!
//MARK: -  variables
    var currentIndex = 0
    var subTitleArr = ["Lorem Ipsum is simply dummy text of the printing and typesetting industry.","Discover the best foods from over 1,000 restaurants and fast delivery to your doorstep","Lorem Ipsum is simply dummy text of the printing and typesetting industry."]
    var landingImgArr = ["landing2","landing1","landing3"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        landingCV.delegate = self
        landingCV.dataSource = self
        pageControl.numberOfPages = 3
        pageControl.hidesForSinglePage = true
    }
    
    @IBAction func skipTap(_ sender: UIButton){
        let vc = Storyboards.main.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
        pushToVC(vc: vc)
    }
    
    @IBAction func nextTap(_ sender: UIButton){
        scrollCollection()
    }
}

extension LandingVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subTitleArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = landingCV.dequeueReusableCell(withReuseIdentifier: "LandingCVC", for: indexPath) as! LandingCVC
        subTitle.text = subTitleArr[indexPath.item]
        cell.landingImage.image = UIImage(named: landingImgArr[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let width = scrollView.frame.width - (scrollView.contentInset.left*2)
        let index = scrollView.contentOffset.x / width
        let roundedIndex = round( index )
//        self.visibleIndex = Int(roundedIndex)
        self.currentIndex = Int(roundedIndex)
        pageControl.currentPage = Int(roundedIndex)
    }
    func scrollCollection(){
        currentIndex += 1
        if currentIndex <= 2{
            self.landingCV.scrollToItem(at: IndexPath(row: currentIndex, section: 0), at: .centeredHorizontally, animated: true)
            self.landingCV.isPagingEnabled = true
            self.landingCV.reloadData()
            pageControl.currentPage = currentIndex
        }else{
            let vc = Storyboards.main.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
            self.pushToVC(vc: vc)
        }
       
    }
    
}
