//
//  ChangePasswordVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 10/05/23.
//

import UIKit

class ChangePasswordVC: UIViewController {

    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var oldPassword: UITextField!
    
    var viewmodel = AuthviewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func updateTapped(_ sender: Any) {
        viewmodel.changePasswordapicall(oldpassword: oldPassword.text ?? "", newpassword: newPassword.text ?? "", confirmpassword: confirmPassword.text ?? "") {
            let refreshAlert = UIAlertController(title: "Change password", message: "Password has been changed sucessfully.", preferredStyle: UIAlertController.Style.alert)

            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                  print("Handle Ok logic here")
                self.pop()
            }))

            self.present(refreshAlert, animated: true, completion: nil)
            
        }
       
    }
    @IBAction func backTapped(_ sender: Any) {
        pop()
    }
    @IBAction func btneyeold(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        oldPassword.isSecureTextEntry = !sender.isSelected
    }
    @IBAction func btneyeNewpass(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        newPassword.isSecureTextEntry = !sender.isSelected
    }
    @IBAction func btnEyeConfirm(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        confirmPassword.isSecureTextEntry = !sender.isSelected
    }
}
