//
//  OtpVerificationVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 10/05/23.
//

import UIKit
import SVPinView

class OtpVerificationVC: UIViewController {
    
    //Mark :--> Outlets
    @IBOutlet weak var btngotosignup: UIButton!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var SvpinVW: SVPinView!
   
    var isSeenDelegate = false
    
//    MARK: - VARIABLE
    var viewmodel = AuthviewModel()
    var otpVerify : String?
    
    override func viewDidLoad() {
        if isSeenDelegate == true {
            viewmodel.ResendOtp(email: Store.userDetails?.body?.email ?? "") { data in
                CommonUtilities.shared.showAlert(message: "Otp has been sent to email successfully", isSuccess: .success)
                self.btngotosignup.isHidden = false
            }
        }

        lblNumber.text = "Please check your mobile number \( Store.userDetails?.body?.email ?? "") continue to reset your password:"
        super.viewDidLoad()
        self.btngotosignup.isHidden = true

    }
    
    //Mark:--> Actions
    @IBAction func btngotosignup(_ sender: Any) {
        let vc = Storyboards.main.instantiateViewController(withIdentifier: "SignUpVC") as! SignUpVC
        self.pushToVC(vc: vc)
    }
    @IBAction func backTapped(_ sender: Any) {
       pop()
    }
    @IBAction func btnNext(_ sender: UIButton) {
        let otp = SvpinVW.getPin()
        viewmodel.otpverification(otp: SvpinVW.getPin()) {
            Store.userDetails?.body?.otpVerified = 1
            if isBusiness{
                let vc = Storyboards.tab.instantiateViewController(withIdentifier: "AddBusinessVC") as! AddBusinessVC
                vc.setTitle = "Business setup"
                self.pushToVC(vc: vc)
            }else{
                let vc = Storyboards.tab.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                CommonUtilities.shared.showAlert(message: "OTP fetched successfully", isSuccess: .success)
                Store.autoLogin = true
                self.pushToVC(vc: vc)
            }
        }
    }
    @IBAction func resendOtp(_ sender: UIButton){
        
        viewmodel.ResendOtp(email: Store.userDetails?.body?.email ?? "") { data in
            CommonUtilities.shared.showAlert(message: "Otp has been sent to email successfully", isSuccess: .success)
//            CommonUtilities.shared.showAlert(message: "Sent Otp Successfully", isSuccess: .success)
//            DispatchQueue.main.async {
//                CommonUtilities.shared.showAlert(Title: "Please enter otp", message:  "\(String(describing: self.otpVerify ?? ""))", isSuccess: .success, duration: 2)
//            }
        }
    }
}

