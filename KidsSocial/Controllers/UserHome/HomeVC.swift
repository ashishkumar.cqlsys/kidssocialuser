//
//  HomeVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 11/05/23.
//

import UIKit
import SDWebImage
import CoreLocation
import AdvancedPageControl

class HomeVC: UIViewController {
    //MARK: -  outlets
    @IBOutlet weak var nodatafoundImage: UIImageView!
    @IBOutlet weak var homeTable: UITableView!
    @IBOutlet weak var searchText: UITextField!
    @IBOutlet weak var bannerCollection: UICollectionView!
    @IBOutlet weak var pagecontrol: AdvancedPageControlView!
    //MARK: - variables
    var viewmodel = UserViewModel()
    var datagetBannerHome = [BannerUser]()
    var datagetBusinessHome = [BusinessUser]()
    var filterData = [BusinessUser]()
    var datagetBusinessBody : UserHomeModelBody?
    var headerArr = ["Categories","Nearby business"]
    var isfrom = Int()
    var longitude = Double()
    var latitude = Double()
    var locationManager = CLLocationManager()
    var Location  = String()
    var arrCat = [BannerUser]()
    var hasPermission = false
    var locationUpdated = Bool()


    
    //MARK: - viewdidload
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        setPageControl()
        
        isfrom = 0
        changingData = 0

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getUpdatedLocation()
//        if (CLLocationManager.locationServicesEnabled())
//        {
//            locationManager.delegate = self
//            locationManager.desiredAccuracy = kCLLocationAccuracyBest
//            locationManager.requestAlwaysAuthorization()
//            locationManager.startUpdatingLocation()
//            locationManager.requestWhenInUseAuthorization()
//        }else{
//            if hasPermission == false {
//                openSettings()
//            }else{
//
//            }
//
//        }
//
    }
    func getUpdatedLocation(){
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
         //   locationManager.distanceFilter = 100.0
            if #available(iOS 14.0, *) {
                locationManager.desiredAccuracy = kCLLocationAccuracyReduced
            } else {
                // Fallback on earlier versions
            }
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        //MARK:- Enable Location Services
        if !hasLocationPermission() {
            let alertController = UIAlertController(title: "Enable Location Services", message: "Enable your location to find near by provider.", preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
                //Redirect to Settings app
                UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
            alertController.addAction(cancelAction)
            
            alertController.addAction(okAction)
            locationManager.delegate = self
            locationManager.requestWhenInUseAuthorization()
            if CLLocationManager.locationServicesEnabled() {
                locationUpdated = true
                locationManager.delegate = self
             //   locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                    locationManager.startUpdatingLocation()
            }else{
                locationUpdated = false
            }
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            @unknown default:
                fatalError()
            }
        } else {
            hasPermission = false
        }
        return hasPermission
    }
    func openSettings(){
        if let url = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url)
        }
    }
//    func hasLocationPermission() -> Bool {
//            var hasPermission = false
//            let manager = CLLocationManager()
//
//            if CLLocationManager.locationServicesEnabled() {
//                switch manager.authorizationStatus {
//                case .notDetermined, .restricted, .denied:
//                    hasPermission = false
//                case .authorizedAlways, .authorizedWhenInUse:
//                    hasPermission = true
//                    //getArea()
//                @unknown default:
//                        break
//                }
//            } else {
//                hasPermission = false
//            }
//
//            return hasPermission
//        }

    override func viewWillLayoutSubviews() {
//            if !hasLocationPermission() {
//                let alertController = UIAlertController(title: "Location Permission Required", message: "Enable your location to find near by provider.", preferredStyle: .alert)
//
//                let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
//
//                    UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
//                })
//
//                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(cnclAction) in
//
//                    //self.navigationController?.popViewController(animated: true)
//                })
//                alertController.addAction(cancelAction)
//
//                alertController.addAction(okAction)
//
//                self.present(alertController, animated: true, completion: nil)
//            }
        }
    func determineMyCurrentLocation() {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            if CLLocationManager.locationServicesEnabled() {
                locationManager.startUpdatingLocation()
            } else{
                if let url = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(url) {
                    UIApplication.shared.open(url)
                }
            }
        }

    func getUserLocation() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.delegate = self
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
            locationManager.allowsBackgroundLocationUpdates = true
        }
    //    MARK: - FUNCTION API
    func setupApi(){
        viewmodel.userHomeApicall(latitude: self.latitude, longitude: self.longitude) { data in
        self.datagetBannerHome = data?.banners ?? []
        self.datagetBusinessHome = data?.businesses ?? []
        self.filterData = data?.businesses ?? []
        self.arrCat = data?.categories ?? []
        self.datagetBusinessBody = data
        self.bannerCollection.reloadData()
        self.homeTable.reloadData()
        }
    }
    //MARK: - nibload
    override func loadView() {
        super.loadView()
        homeTable.register(UINib(nibName: "HomeTableCell", bundle: nil), forCellReuseIdentifier: "HomeTableCell")
        bannerCollection.register(UINib(nibName: "BannerCollectionCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionCell")
    }
    //MARK: - PAGE CONTROLLER
    func setPageControl(){
        pagecontrol.isHidden = true
        pagecontrol.drawer = ExtendedDotDrawer(numberOfPages: 3 ,  height: 6, width: 6, space: 5, raduis: 10, currentItem: 0, indicatorColor: UIColor(named: "themeColor"), dotsColor: .white, isBordered: false, borderColor: .clear, borderWidth: 0, indicatorBorderColor: .clear, indicatorBorderWidth: 0)
        //        pagecontrol.numberOfPages = bannerArr.count
        
    }
    //    MARK: - scrollViewDidScroll
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        
        pagecontrol.setPageOffset(offSet/width)
        
    }
    //MARK: -  actions
    @IBAction func searchTapped(_ sender: Any) {
       
    }
    
    @IBAction func btnsearch(_ sender: Any) {
        self.tabBarController?.selectedIndex = 2

    }
    
    @IBAction func notificationTapped
    (_ sender: Any) {
        
        if UserDefaults.standard.bool(forKey: "ok") == false{
            let alert = UIAlertController(title: "Login", message: "Please login first", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Login", style: .default, handler: { action in
                let vc = Storyboards.main.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                self.pushToVC(vc: vc)
                
                
            })
            alert.addAction(ok)
            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in self.dismiss(animated: true, completion: nil)
            })
            alert.addAction(cancel)
            DispatchQueue.main.async(execute: {
                self.present(alert, animated: true)
            })
            
        }else{
            let vc = Storyboards.profile.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
            self.pushToVC(vc: vc)
        }
    }
    
    @IBAction func settingsTapped(_ sender: Any) {
        if Store.userDetails == nil {
            if UserDefaults.standard.bool(forKey: "ok") == false{
                let alert = UIAlertController(title: "Login", message: "Please login first", preferredStyle: .alert)
                let ok = UIAlertAction(title: "Login", style: .default, handler: { action in
                    let vc = Storyboards.main.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.pushToVC(vc: vc)
                    
                    
                })
                alert.addAction(ok)
                let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in self.dismiss(animated: true, completion: nil)
                })
                alert.addAction(cancel)
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true)
                })
                
            }
        }else {
            let vc = Storyboards.profile.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
            self.pushToVC(vc: vc)
        }
//        if UserDefaults.standard.bool(forKey: "ok") == false{
//            let alert = UIAlertController(title: "Login", message: "Please login first", preferredStyle: .alert)
//            let ok = UIAlertAction(title: "Login", style: .default, handler: { action in
//                let vc = Storyboards.main.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//                self.pushToVC(vc: vc)
//
//
//            })
//            alert.addAction(ok)
//            let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in self.dismiss(animated: true, completion: nil)
//            })
//            alert.addAction(cancel)
//            DispatchQueue.main.async(execute: {
//                self.present(alert, animated: true)
//            })
//
//        }else{
//            let vc = Storyboards.profile.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
//            self.pushToVC(vc: vc)
//        }
     
    }
    
    //MARK: - get tab bar item
        func getItem(title:String = "",active:String,inactive:String)-> UITabBarItem{
            let icon = UITabBarItem(title: title, image: UIImage(named: inactive), selectedImage: UIImage(named: active))
            return icon
        }
    }

    

//MARK: - collection datasource delegate
extension HomeVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return headerArr.count
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCell") as! HomeTableCell
        
        if indexPath.row == 0{
            cell.viewAllBtn.isHidden = true
        }else{
            cell.viewAllBtn.isHidden = false
        }
        cell.callBackTable = {
            
        }
        cell.callbackSecond = {
            let vc = Storyboards.main.instantiateViewController(withIdentifier: "homelistVC") as! homelistVC
            self.pushToVC(vc: vc)
        }
        
        cell.index = indexPath.row
        cell.title.text = headerArr[indexPath.row]
        cell.locationLbl.text = datagetBusinessHome.first?.location ?? ""
       
        cell.locationLbl.isHidden = indexPath.row == 0 ? true:false
        cell.vc = self
        cell.isFromCategory = false
        
        
        cell.SetData(listingData: self.datagetBusinessBody)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 220:200
    }
    
}

//MARK: - collection datasource delegate
extension HomeVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isfrom == 0{
            let count =  datagetBannerHome.count
            pagecontrol.numberOfPages = count
            pagecontrol.isHidden = count == 1 ? true :  false
            return count
            
        }else {
            let count  = datagetBannerHome.count
            pagecontrol.numberOfPages = count
            pagecontrol.isHidden = count == 1 ? true :  false
            return count
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = datagetBannerHome[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionCell", for: indexPath) as! BannerCollectionCell
        cell.image.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        cell.image.setUserImage(URL(string: bannerUrl + (data.banner ?? "")))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        
    }
}
//MARK:- LOCATION
extension HomeVC : CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        self.latitude = locValue.latitude
        self.longitude = locValue.longitude
        self.setupApi()
        locationManager.stopUpdatingLocation()
    }
}
