//
//  SubCategoryCVCone.swift
//  KidsSocial
//
//  Created by cql131 on 22/06/23.
//

import UIKit

class SubCategoryCVCone: UICollectionViewCell {
    
    @IBOutlet weak var lblcategory: UILabel!
    @IBOutlet weak var lblSubCategory: UILabel!
    @IBOutlet weak var subCategoryImage: UIImageView!
}
