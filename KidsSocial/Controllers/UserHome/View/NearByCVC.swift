//
//  NearByCVC.swift
//  KidsSocial
//
//  Created by cql131 on 22/06/23.
//

import UIKit

class NearByCVC: UICollectionViewCell {
    @IBOutlet weak var BusinessImage: UIImageView!
    @IBOutlet weak var businessname: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    
    @IBOutlet weak var btnLike: UIButton!
    @IBOutlet weak var btnViewBusiness: UIButton!
    
//    MARK:- VARIABLE
    var callback: (()->())?
    var callbacksecond: (()->())?
    @IBAction func btnViewBusiness(_ sender: UIButton) {
        callbacksecond?()
    }
//    @IBAction func btnLike(_ sender: UIButton) {
//        callback?()
//        sender.isSelected = !sender.isSelected
//    }
}
