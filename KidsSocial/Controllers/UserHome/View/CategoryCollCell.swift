//
//  CategoryCollCell.swift
//  KidsSocial
//
//  Created by AjayDhiman on 11/05/23.
//

import UIKit

class CategoryCollCell: UICollectionViewCell {

    @IBOutlet weak var categoryName: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
