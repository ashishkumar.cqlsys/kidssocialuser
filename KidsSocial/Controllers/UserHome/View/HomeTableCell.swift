//
//  HomeTableCell.swift
//  KidsSocial
//
//  Created by AjayDhiman on 11/05/23.
//
var changingData = Int()
import UIKit
import DropDown
import SDWebImage
import AVFAudio
import CoreLocation

class HomeTableCell: UITableViewCell {
    //MARK: - outlets
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var categoryCV: UICollectionView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var viewAllBtn: UIButton!
    
    //MARK: - variables
    var imageArr = ["Play","art","sports","dayCare","afterSchool","party"]
    var catTextArr = ["Play","Art/music/dance","Sports","Daycare","Afterschool","Parties & Events"]
    var viewmodel = UserViewModel()
    var datagetBannerHome = [BannerUser]()
    var arrCat = [BannerUser]()
    var datagetBusinessHome = [BusinessUser]()
    var homeData: UserHomeModelBody?
    
    var catImgArr = ["house","playtime","waterSlide","city","livestock","museum"]
    var catArr = ["Play","Indoor Play","Water parks","City parks","Around the city","Museums"]
    var viewmodelSub = BuisnessviewModel()
    var index = 0
    var isFromCategory = false
    var modelsecond : [SubcategoryModelBody]?
    var vc = UIViewController()
    var datagetCat = Int()
    var longitude = Double()
    var latitude = Double()
    let locationManager = CLLocationManager()
    var callBackTable: (() -> ())?
    var callbackSecond : (()->())?
    
    //MARK: -   awakeFromNib()
    override func awakeFromNib() {
        super.awakeFromNib()
      //  setupApi()
        //setupsubcategory()
        load()
        categoryCV.delegate = self
        categoryCV.dataSource = self
        
        if index == 0{
            viewAllBtn.isHidden = true
        }else{
            viewAllBtn.isHidden = false
        }
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        locationManager.requestWhenInUseAuthorization()
    }
    
    
    func SetData(listingData: UserHomeModelBody?){
        self.homeData = listingData
        self.datagetBannerHome = listingData?.banners ?? []
        self.datagetBusinessHome = listingData?.businesses ?? []
        self.arrCat = listingData?.categories ?? []
        self.categoryCV.reloadData()
    }
    
    //    MARK: - FUNCTION API
//    func setupApi(){
//        //Latitude: 30.6800 Longitude: 76.7221.
//        viewmodel.userHomeApicall(latitude: self.latitude, longitude: self.longitude) { data in
//            self.datagetBannerHome = data?.banners ?? []
//            self.datagetBusinessHome = data?.businesses ?? []
//            self.arrCat = data?.categories ?? []
//            self.homeData = data
//            self.categoryCV.reloadData()
//            
//        }
//    }
    //MARK: -  nibLoad
    func load() {
        //
        categoryCV.register(UINib(nibName: "CategoryCollCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCollCell")
        //
        categoryCV.register(UINib(nibName: "BusinessDetailCollCell", bundle: nil), forCellWithReuseIdentifier: "BusinessDetailCollCell")
        //
        categoryCV.register(UINib(nibName: "HomeBusinessCVC", bundle: nil), forCellWithReuseIdentifier: "HomeBusinessCVC")
        //
        
        
    }
    @IBAction func btnViewall(_ sender: Any) {
        callbackSecond?()
    }
}

//MARK: - HomeTableCell
extension HomeTableCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if isFromCategory{
            if index == 0{
                return homeData?.categories?.count ?? 0
            }else{
                return homeData?.businesses?.count ?? 0
            }
        }else{
            if index == 0{
                return homeData?.categories?.count ?? 0
            }else{
                return homeData?.businesses?.count ?? 0
            }
        }
    }
    
    //MARK: - CategoryCollCell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // if changingData == 0{
        if index == 0{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollCell", for: indexPath) as! CategoryCollCell
            let data = homeData?.categories?[indexPath.row]
            
            cell.categoryName.text = data?.name ?? ""
            cell.categoryImage.setUserImage(URL(string: categoryURl + (data?.image ?? "")))
//            cell.categoryImage.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            
            return cell
        }else{
            //MARK: - BusinessDetailCollCell
            if isFromCategory{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BusinessDetailCollCell", for: indexPath) as! BusinessDetailCollCell
                let data = homeData?.businesses?[indexPath.item]
                cell.businessName.text = data?.name
                cell.businessImage.setUserImage(URL(string: imageURL + (data?.image ?? "")))
                
                //                        cell.businessImage.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                cell.viewBusinessBtn.addTarget(self, action: #selector(viewBusiness(_:)), for: .touchUpInside)
                cell.role = "User"
                cell.callBack = { data in
                    if data == "like"{
                        if cell.likeBtn.currentImage == UIImage(named: "like"){
                            cell.likeBtn.setImage(UIImage(named: "dislike"), for: .normal)
                        }else{
                            cell.likeBtn.setImage(UIImage(named: "like"), for: .normal)
                        }
                    }
                }
                return cell
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeBusinessCVC", for: indexPath) as! HomeBusinessCVC
                let data = homeData?.businesses?[indexPath.item]
                cell.businessTitle.text = data?.name
                
                cell.businessDesc.text = data?.location
                cell.businessImage.setUserImage(URL(string: imageURL + (data?.image ?? "")))
                return cell
            }
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isFromCategory{
            if index == 0{
                return CGSize(width: collectionView.frame.width/4.5, height: collectionView.frame.height)
            }else{
                return CGSize(width: collectionView.frame.width/1.4, height: collectionView.frame.height/2)
            }
        }else{
            if index == 0{
                return CGSize(width: collectionView.frame.width/3, height: collectionView.frame.height/2)
            }else{
                return CGSize(width: collectionView.frame.width/1.2, height: collectionView.frame.height)
            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      
        if index == 0{
            let controller = Storyboards.Other.instantiateViewController(withIdentifier: "CategoryVC") as! CategoryVC
            controller.getcatid = arrCat[indexPath.row].id ?? 0
            vc.navigationController?.pushViewController(controller, animated: true)
            
        }else{
            if !isFromCategory{
                let data = datagetBusinessHome[indexPath.row]
                let controller = Storyboards.Other.instantiateViewController(withIdentifier: "BussinessDetailVC") as! BussinessDetailVC
                //                        controller.businessid = data[indexPath.row].id ?? 0
                controller.businessid = data.id ?? 0
                parentView?.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    @objc func viewBusiness(_ sender: UIButton) {
        let controller = Storyboards.Other.instantiateViewController(withIdentifier: "BussinessDetailVC") as! BussinessDetailVC
        parentView?.navigationController?.pushViewController(controller, animated: true)
    }
}
extension HomeTableCell : CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        self.latitude = locValue.latitude
        self.longitude = locValue.longitude
     //    setupApi()
        locationManager.stopUpdatingLocation()
    }
}
