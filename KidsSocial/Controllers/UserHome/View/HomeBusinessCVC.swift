//
//  HomeBusinessCVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 11/05/23.
//

import UIKit

class HomeBusinessCVC: UICollectionViewCell {
    @IBOutlet weak var businessView: ShadowView!
    @IBOutlet weak var businessTitle: UILabel!
    
    @IBOutlet weak var businessImage: UIImageView!
    @IBOutlet weak var businessDesc: UILabel!
    
    //MARK: --- VARIABLES
    var callBack: (() -> ())?
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
    }

}
