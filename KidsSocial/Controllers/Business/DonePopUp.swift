//
//  DonePopUp.swift
//  KidsSocial
//
//  Created by AjayDhiman on 16/05/23.
//

import UIKit

class DonePopUp: UIViewController {
    var callBack: (successResponse)?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func donetapped(_ sender: Any) {
        dismiss(animated: true) {
            self.callBack?()
        }
    }

}
