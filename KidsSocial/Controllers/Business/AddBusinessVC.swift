//
//  AddBusinessVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 11/05/23.
//

import UIKit
import DropDown
import GooglePlaces
import AVFoundation
import SwiftUI
import SDWebImage

class AddBusinessVC: UIViewController , UINavigationControllerDelegate, UIAdaptivePresentationControllerDelegate, UITabBarControllerDelegate,UITabBarDelegate{
    //MARK: - outlets
    @IBOutlet weak var tfZip: UITextField!
    @IBOutlet weak var tfcity: UITextField!
    @IBOutlet weak var tfstate: UITextField!
    @IBOutlet weak var categoryView: TextfeildView!
    @IBOutlet weak var subCatView: TextfeildView!
    @IBOutlet weak var businessImage: UIImageView!
    @IBOutlet weak var businessVdThumb: UIImageView!
    @IBOutlet weak var screenTitle: HeaderLabel!
    @IBOutlet weak var descriptionTV: UITextView!
    @IBOutlet weak var locationTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var subcatTF: UITextField!
    @IBOutlet weak var categoryTF: UITextField!
    @IBOutlet weak var businessName: UITextField!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    
    //MARK: - variables
    var businessObj : Business?
    
    
    var setTitle = "Add Business"
    var selectedImage = UIImage()
    var drop = DropDown()
    var category = ["Play","Art/music/dance","Sports","Daycare","Afterschool","Parties & Events"]
    var subCategory = ["Indoor Playground","Baby play","Water parks","City parks","Animals","Museums"]
    var viewmodel = BuisnessviewModel()
    var model = [CategoryModelBody]()
    var modelsecond = [SubcategoryModelBody]()
//    var VideoStruct : ImageStructInfo?
    var catID = Int()
    var subcategoryID = Int()
    var getLogitude = String()
    var getLatitude = String()
    var lat:Double?
    var long:Double?
    var imageInfo : ImageStructInfo?
    var videoUrl : URL?
    var statuschange = 1
    var businessDetailsModel : BusinessDetailsModelBody?
    var dataOrignal : Original?
    var datasimilar = [Original]()
    var businessID = Int()
    var isImageSelected = false
    var isVideoSelect = false
    
    
    //MARK: -  viewdidload
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.delegate = self
        setup()
        locationTF.delegate = self
        tfcity.delegate = self
        tfstate.delegate = self
        tfZip.delegate = self
        subcatTF.delegate = self
        categoryTF.delegate = self
//        setTextView()
//        descriptionTV.delegate = self
        screenTitle.text = setTitle
        if setTitle == "Add Business"{
            backBtn.isHidden = true
            submitBtn.setTitle("Submit", for: .normal)
        }else{
            if statuschange == 1 {
                setTitle = "Add Business"
                submitBtn.setTitle("Submit", for: .normal)
                backBtn.isHidden = false
            }else{
                submitBtn.setTitle("Update", for: .normal)
                backBtn.isHidden = false
            setupBusinesssdata()
               
                setTitle = "Edit Business"
            }
        }

          }
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 {
            self.locationTF.text = ""
            self.descriptionTV.text = ""
            self.phoneTF.text = ""
            self.subcatTF.text = ""
            self.categoryTF.text = ""
            self.businessName.text = ""
            self.tfcity.text = ""
            self.tfZip.text = ""
            self.tfstate.text = ""
            self.businessImage.image = UIImage.init(named: "")
            self.businessVdThumb.image = UIImage.init(named: "")

        }
    }
    
    //MARK: - GooGLE PLACE AUTOCOMPLETE
    func autocompleteClicked() {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        
        // Specify the place data types to return.
        let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt64(UInt(GMSPlaceField.name.rawValue) | UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue)))
        autocompleteController.placeFields = fields
        
        // Specify a filter.
        let filter = GMSAutocompleteFilter()
        filter.type = .address
        autocompleteController.autocompleteFilter = filter
        
        // Display the autocomplete view controller.
        present(autocompleteController, animated: true, completion: nil)
    }
    
    //MARK: -  CATEGORY
    func setup(){
        viewmodel.categoryapicall { data in
            self.model = data ?? []
            //            self.setupsubcategory()
        }
    }
    //    MARK: - SUB CATEGORY
    func setupsubcategory(subID:Int) {
        viewmodel.subcategoryapicall(categoryId: subID) { data in
            self.modelsecond = data ?? []
        }
        
    }
    //    MARK: - EDIT BUSINESS
    
    func setupBusinesssdata(){
            viewmodel.buisnessDetailsapicall(business_id: businessID) { data in
                self.businessDetailsModel = data
                self.dataOrignal = data?.original
    //            self.tfcity.text = data?.original?.longitude
    //            self.tfZip.text = data?.original?.latitude
    //            self.tfstate.text = data?.original?.longitude
                self.locationTF.text = data?.original.location
                self.descriptionTV.text = data?.original.shortDescription
                self.phoneTF.text = "\(data?.original.phone ?? 0)"
                self.subcatTF.text = self.dataOrignal?.categoryName ?? ""
                self.categoryTF.text = self.dataOrignal?.subcategoryName ?? ""
                self.businessName.text = data?.original.name
                self.getLatitude = data?.original.latitude ?? ""
                self.getLogitude = data?.original.longitude ?? ""
                self.catID = data?.original.categoryID ?? 0
                self.subcategoryID = data?.original.subCategoryID ?? 0
                self.businessImage.setUserImage(URL(string: imageURL + (data?.original.image ?? "")))
                self.businessImage.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                self.businessVdThumb.setUserImage(URL(string: thambnailUrl + (data?.original.thumbnail ?? "")))
              self.businessVdThumb.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
                self.getAddressFromLoc(pdblLatitude: data?.original.latitude ?? "", withLongitude: data?.original.longitude ?? "")
//               }
        }

    }

    //     MARK: - FUNCTION OF UPLOAD VIDEO
    func uploadVideo(_ videoData: Data) {
        // Create an HTTP POST request
        guard let url = URL(string: "your_api_endpoint") else {
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // Set headers if needed
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // Set the video data as the request body
        request.httpBody = videoData
        
        // Send the request using URLSession or any networking library of your choice
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            // Handle the response and error
        }
        task.resume()
    }
    
    
    func getAddressFromLoc(pdblLatitude: String, withLongitude pdblLongitude: String){
        
            var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
            let lat: Double = Double("\(pdblLatitude)") ?? 00
            //21.228124
            let lon: Double = Double("\(pdblLongitude)") ?? 00
            //72.833770
            let ceo: CLGeocoder = CLGeocoder()
            center.latitude = lat
            center.longitude = lon

            let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)


            ceo.reverseGeocodeLocation(loc, completionHandler:
                                        {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                if placemarks != nil
                {
                    let pm = placemarks! as [CLPlacemark]

                    if pm.count > 0 {
                        let pm = placemarks![0]

                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                            self.tfcity.text = pm.locality
                            self.tfstate.text = pm.administrativeArea
                           
                            
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                            self.tfZip.text = pm.postalCode ?? ""
                        }

                    }
                }
            })
        }

    //MARK: -  ACTIONS
    @IBAction func addVideoTap(_ sender: Any) {
        self.checkCameraAccess()
        imagePickerClass(true, false, false).showPicker(self) { (image, type, url) in
            let formatter = DateFormatter()
            formatter.dateFormat = dateFormat.fullDate.rawValue
            let date = formatter.string(from: Date())
            self.isVideoSelect = true
            self.videoUrl = url
            self.businessVdThumb.image = image
            do{
                let videoData: Data = try Data(contentsOf: url, options: Data.ReadingOptions.alwaysMapped)
                let videoInfo : ImageStructInfo
                videoInfo = ImageStructInfo.init(fileName: "Video\(date).mp4", type: "video/mp4", data: videoData, key: "video", image: image)
                self.imageInfo = videoInfo
            }catch{
                print("Unable to load data: \(error)")
            }
            
        }
    }
    
    @IBAction func addimgTapped(_ sender: Any) {
        self.checkCameraAccess()
        imagePickerClass(false, false, true).showPicker(self) { (image, type, url) in
            self.isImageSelected = true
            self.businessImage.image = image
        }
    }
    
    @IBAction func categoryBtn(_ sender: UIButton){
        
        resignResponder()
        drop.anchorView = categoryView
        drop.dataSource = model.map{$0.name ?? ""}
        drop.show()
        drop.selectionAction = {[unowned self] (index,item) in
            categoryTF.text = item
            print(model[index].id)
            catID = model[index].id ?? 0
            self.setupsubcategory(subID: model[index].id ?? 0)
            drop.hide()
            subcatTF.text = ""
        }
    }
    
    @IBAction func subCatBtn(_ sender: UIButton){
        // self.setupsubcategory()
        resignResponder()
        drop.anchorView = subCatView
        drop.dataSource = modelsecond.map{$0.subcatName ?? ""}
        drop.show()
        drop.selectionAction = {[unowned self] (index,item) in
            subcatTF.text = item
            print(modelsecond[index].id)
            subcategoryID  = modelsecond[index].id ?? 0
            drop.hide()
        }
    }
    //
    @IBAction func backTapped(_ sender: Any) {
        pop()
    }
    //
    @IBAction func submitTapped(_ sender: Any) {
        if statuschange == 1 {
            viewmodel.addbuisness(isImageSelected:self.isImageSelected,isVideoSelect:self.isVideoSelect,name: businessName.text ?? "" , phone: phoneTF.text ?? "", short_description: descriptionTV.text ?? "", location: locationTF.text ?? "" , latitude: Double(getLatitude) ?? 0.0  , longitude: Double(getLogitude) ?? 0.0, image: businessImage.image ?? UIImage(), category_id: catID, sub_category_id: subcategoryID, video: imageInfo)  {
                let vc = Storyboards.business.instantiateViewController(withIdentifier: "DonePopUp") as! DonePopUp
                vc.modalPresentationStyle = .overCurrentContext
                vc.callBack = {
                    let vc = Storyboards.tab.instantiateViewController(withIdentifier: "BusinessTabBarVC") as! BusinessTabBarVC
                    Store.autoLogin = true
                    self.pushToVC(vc: vc)
                }
                self.present(vc, animated: true, completion: nil)
            }
        }else{
            viewmodel.editbuisness(name: businessName.text ?? "", phone: phoneTF.text ?? "", short_description: descriptionTV.text ?? "", location: locationTF.text ?? "" , latitude: Double(getLatitude) ?? 0.0 , longitude: Double(getLogitude) ?? 0.0, image: businessImage.image ?? UIImage(), category_id: catID, sub_category_id: subcategoryID, business_id: businessID, video: imageInfo ?? ImageStructInfo(fileName: "", type: "", data: Data(), key: "", image: UIImage())) {
                let vc = Storyboards.tab.instantiateViewController(withIdentifier: "BusinessTabBarVC") as! BusinessTabBarVC
//                Store.autoLogin = true
                self.pushToVC(vc: vc)
            }
        }
   
    }
    
}
//MARK: - EXTENISONS Google PLACES
extension AddBusinessVC: UITextFieldDelegate{
    
    func resignResponder(){
        businessName.resignFirstResponder()
        phoneTF.resignFirstResponder()
        locationTF.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == locationTF {
            autocompleteClicked()
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            
            // Specify the place data types to return.
            let fields: GMSPlaceField = GMSPlaceField(rawValue: UInt64(UInt(GMSPlaceField.name.rawValue) | UInt(GMSPlaceField.placeID.rawValue) | UInt(GMSPlaceField.coordinate.rawValue)))
            autocompleteController.placeFields = fields
            
            // Specify a filter.
            let filter = GMSAutocompleteFilter()
            filter.type = .address
            autocompleteController.autocompleteFilter = filter
            
            // Display the autocomplete view controller.
            present(autocompleteController, animated: true, completion: nil)
        }
    }
}



extension AddBusinessVC: GMSAutocompleteViewControllerDelegate{
    // Handle the user's selection.
    func getAddressFromLatLon(pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
       
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
                                    {(placemarks, error) in
            if (error != nil)
            {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            }
            let pm = placemarks! as [CLPlacemark]
            
            if pm.count > 0 {
                let pm = placemarks![0]
                var addressString : String = ""
                if pm.subLocality != nil {
                    addressString = addressString + (pm.subLocality ?? "") + ", "
                }
                if pm.thoroughfare != nil {
                    addressString = addressString + (pm.thoroughfare ?? "") + ", "
                }
                if pm.locality != nil {
                    addressString = addressString + (pm.locality ?? "") + ", "
                    
                }
                if pm.country != nil {
                    addressString = addressString + (pm.country ?? "") + ", "
                    
                }
                if pm.postalCode != nil {
                    addressString = addressString + (pm.postalCode ?? "") + " "
                }
                if pm.subLocality != nil{
                    addressString = addressString + (pm.administrativeArea ?? "") + ", "
                }
                print(addressString)
                //                self.txtFldCountry.text = pm.country
                self.tfcity.text = pm.locality
                self.tfstate.text = pm.administrativeArea
                self.tfZip.text = pm.postalCode
            }
        })
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        print("Place name: -----   \(place.name ?? "")")
        self.dismiss(animated: true) {
            self.locationTF.text = place.name
            self.getAddressFromLatLon(pdblLatitude: "\(place.coordinate.latitude)", withLongitude: "\(place.coordinate.longitude)")
            self.getLatitude = "\(place.coordinate.latitude)"
            
            self.getLogitude = "\(place.coordinate.longitude)"
        }
    }
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}


//MARK: - CAMERA ACCSESS
extension AddBusinessVC {
    func checkCameraAccess() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            print("Denied, request permission from settings")
            presentCameraSettings()
        case .restricted:
            print("Restricted, device owner must approve")
        case .authorized:
            print("Authorized, proceed")
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    print("Permission granted, proceed")
                } else {
                    print("Permission denied")
                }
            }
        default: print("kkkkk")
        }
    }
    
    func presentCameraSettings() {
        let alertController = UIAlertController(title: "Error",
                                                message: "Camera access is denied",
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default))
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    // Handle
                })
            }
        })
        
        present(alertController, animated: true)
    }
}
