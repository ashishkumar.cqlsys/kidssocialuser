//
//  DeletePopUp.swift
//  KidsSocial
//
//  Created by AjayDhiman on 16/05/23.
//

import UIKit

class DeletePopUp: UIViewController {
    
//  MARK: - VARIABLE
    var callBack : ((Bool)->())?
    var viewmodel = BuisnessviewModel()
    var businessDeleteId = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    @IBAction func yesTapped(_ sender: UIButton){
        
        
            self.dismiss(animated: false) {
                self.callBack?(true)
          
        }
    }
    @IBAction func noTapped(_ sender: UIButton){
        dismiss()
    }
}
