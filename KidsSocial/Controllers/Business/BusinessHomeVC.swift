//
//  BusinessHomeVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 16/05/23.
//

import UIKit
import AdvancedPageControl
import SwiftUI
import SDWebImage

class BusinessHomeVC: UIViewController {
    //MARK: - outlet
    @IBOutlet weak var imgNodata: UIImageView!
//    @IBOutlet weak var busHometblHgt: NSLayoutConstraint!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var bannnerCollVw: UICollectionView!
    @IBOutlet weak var pager: AdvancedPageControlView!
    @IBOutlet weak var businessHomeTV: UITableView!
    //MARK: - variables
    var selectedIndex = -1
    var bannerArr = ["bannerImg","1","bannerImg"]
    var viewmodel = BuisnessviewModel()
    var datagetBanner = [Banner]()
    var datagetBusiness = [Business]()
    var status = Int()
    var refreshControl = UIRefreshControl()
//    var imageView: UIImageView?
    
    //MARK: - viewdidload
    override func viewDidLoad() {
//        isBusiness = true
        super.viewDidLoad()
        
       
//               refreshControl.addTarget(self, action: #selector(doSomething), for: .valueChanged)
//            businessHomeTV.refreshControl = refreshControl
//
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        self.refreshControl = UIRefreshControl()
        self.refreshControl.attributedTitle = NSAttributedString(string: "Refresing Newsfeed")
        self.refreshControl.addTarget(self, action: #selector(doSomething), for: .valueChanged)
        self.businessHomeTV.addSubview(refreshControl)

        
        
        self.setPageControl()
    }
    
    @objc func doSomething(refreshControl: UIRefreshControl) {
        setupApi()
        refreshControl.endRefreshing()
    }
    //  MARK: - API HOME NEAR BY
    func setupApi(){
        viewmodel.buisnessHomeApiCall { data in
            DispatchQueue.main.async {
                self.datagetBanner = data?.banners ?? []
                self.datagetBusiness = data?.businesses?.reversed() ?? []
                self.businessHomeTV.reloadData()
                self.bannnerCollVw.reloadData()
            }
            
        }
    }
    //    MARK: - FUNCTION PAGE CONTROLLOR
    func setPageControl(){
        pageControl.isHidden = true
        pager.drawer = ExtendedDotDrawer(numberOfPages: bannerArr.count, height: 6, width: 6, space: 5, raduis: 10, currentItem: 0, indicatorColor: UIColor(named: "themeColor"), dotsColor: .white, isBordered: false, borderColor: .clear, borderWidth: 0, indicatorBorderColor: .clear, indicatorBorderWidth: 0)
        pager.numberOfPages = bannerArr.count
        
    }
    
    //MARK: - viewwillappear
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        view.layoutIfNeeded()
        self.setupApi()
//        busHometblHgt.constant = businessHomeTV.contentSize.height
    }
    
    //MARK: - loadView
    override func loadView() {
        super.loadView()
        bannnerCollVw.register(UINib(nibName: "BannerCollectionCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionCell")
        businessHomeTV.register(UINib(nibName: "BusinessDetailCell", bundle: nil), forCellReuseIdentifier: "BusinessDetailCell")
    }
    //MARK: - actions
    @IBAction func notificationTapped(_ sender: Any) {
        let vc = Storyboards.profile.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.pushToVC(vc: vc)
    }
    
}
//MARK: - table delegate datasource

extension BusinessHomeVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return datagetBusiness.count
//
//                if count == 0 {
//                    imgNodata.image = UIImage.gif(name: "Nodatafound")
//                    imgNodata.isHidden = false
//                } else {
//                    imgNodata.isHidden = true
//                }
//
//                return count
//        let count = datagetBusiness.count ?? 0
//        if count == 0 {
//            if imageView == nil {
//                imageView = createCenteredImageView(image: UIImage.gif(name: "nodataFound"), width: 300, height: 300)
//            }
//        } else {
//            imageView?.removeFromSuperview()
//            imageView = nil
//        }
//        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessDetailCell", for: indexPath) as! BusinessDetailCell
        let data = datagetBusiness[indexPath.row]
        cell.businessName.text = data.name
        cell.locationlbl.text = data.location
        cell.activeButton.tag = indexPath.row
        cell.viewBusinessBtn.tag = indexPath.row
        cell.Businessimage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.Businessimage.setUserImage(URL(string: imageURL + (data.image ?? "")))
        cell.businessColl.isHidden = true
        cell.businessView.isHidden = true
        cell.optionView.isHidden = true
        cell.bussnessBtnHgt.constant = 0
        cell.likeBtn.setImage(UIImage(named: "optionIC"), for: .normal)
        cell.selectionStyle = .none
        
        if  data.isApproved == 0{
            cell.lblAdminApproval.isHidden = false
            cell.lbladminapprovalHieght.constant = 22
            
        }else {
            cell.lblAdminApproval.isHidden = true
            cell.lbladminapprovalHieght.constant = 0
        }
        
        if selectedIndex == indexPath.row{
            cell.optionView.isHidden = false
        }else{
            cell.optionView.isHidden = true
        }
        if data.status == 1{
            cell.activeButton.setTitle("Active", for: .normal)
            cell.activeButton.setImage(UIImage.init(named: "greenDot"), for: .normal)
        }else{
            cell.activeButton.setTitle("Inactive", for: .normal)
            cell.activeButton.setImage(UIImage.init(named: "redDot"), for: .normal)
        }
        
        cell.callbackActive = {
            let status = data.status == 0 ? 1 : 0
            self.viewmodel.activeinactiveapicall(status: status, business_id: self.datagetBusiness[indexPath.row].id ?? 0) { data in
                self.setupApi()
            }
        }
        
        cell.callBack = {[self]data in
            tableView.reloadData()
            if data == "option"{
                if selectedIndex == indexPath.row{
                    selectedIndex = -1
                }else{
                    selectedIndex = indexPath.row
                    
                }
                tableView.reloadData()
            }else if data == "edit"{
                selectedIndex = -1
                editAction(index: indexPath.row)
            }else{
                selectedIndex = -1
                deleteAction(index: indexPath.row)
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = -1
        let vc = Storyboards.Other.instantiateViewController(withIdentifier: "BussinessDetailVC") as! BussinessDetailVC
        pushToVC(vc: vc)
//        isBusiness = true
        vc.businessid = datagetBusiness[indexPath.row].id ?? 0
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        busHometblHgt.constant = tableView.contentSize.height-100
    }

  
}

//MARK: - collectiondelegateDatasource
extension BusinessHomeVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datagetBanner.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionCell",for: indexPath) as! BannerCollectionCell
        let data = datagetBanner[indexPath.row]
        cell.image.sd_imageIndicator = SDWebImageActivityIndicator.gray
        let image = "\(bannerUrl + (data.banner ?? ""))"
        let urlString3 = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        cell.image.sd_setImage(with: URL.init(string: urlString3 ), placeholderImage: UIImage.init(named: "user"), completed: nil)
        
        
        
//        cell.image.setUserImage(URL(string: imageURL + (data.banner ?? "")))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        pageControl.numberOfPages = collectionView.numberOfItems(inSection: 0)
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        
        pager.setPageOffset(offSet/width)
        
        let index = Int(round(offSet/width))
        pager.setPage(index)
        
    }
    //MARK: - edit button action
    func editAction(index:Int){
        let data = datagetBusiness[index]
        let vc = Storyboards.tab.instantiateViewController(withIdentifier: "AddBusinessVC") as! AddBusinessVC
        vc.setTitle = "Edit business"
        vc.statuschange = 0
        vc.businessObj = data
        vc.businessID = datagetBusiness[index].id ?? 0
        print(datagetBusiness[index].id ?? 0)
        self.pushToVC(vc: vc)
    }
    //MARK: - delete button Action
    func deleteAction(index:Int){
        
        let vc = Storyboards.business.instantiateViewController(withIdentifier: "DeletePopUp") as! DeletePopUp
        vc.callBack =  { [weak self] dataa in
            if dataa == true{
                self?.viewmodel.businessdalete(business_id: self?.datagetBusiness[index].id ?? 0) { (getData) in
                    self?.datagetBusiness.remove(at: index)
                    self?.businessHomeTV.reloadData()
                }
            }
            
        }
        
        vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
}
