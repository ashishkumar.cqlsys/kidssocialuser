//
//  BusinessDetailCell.swift
//  KidsSocial
//
//  Created by AjayDhiman on 11/05/23.
//

import UIKit

class BusinessDetailCell: UITableViewCell {
    @IBOutlet weak var lbladminapprovalHieght: NSLayoutConstraint!
    @IBOutlet weak var lblAdminApproval: UILabel!
    @IBOutlet weak var Businessimage: UIImageView!
    @IBOutlet weak var locationlbl: UILabel!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var businessView: UIView!
    @IBOutlet weak var businessColl: UICollectionView!
    @IBOutlet weak var bussnessBtnHgt: NSLayoutConstraint!
    @IBOutlet weak var viewBusinessBtn: ThemeButton!
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var activeButton: UIButton!
    var callbacksecond : (()->())?
    var callBack: ((String)->())?
    var callbackActive: (()->())?
    var datagetBusinessHome = [BusinessUser]()
    var datagetApi = [FavListingtModelBody]()
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        likeBtn.backgroundColor = .lightGray
        businessColl.delegate = self
        businessColl.dataSource = self
        businessView.backgroundColor = .white
        businessView.layer.cornerRadius = 20
        

        if isBusiness{
            businessView.isHidden = false
            lblAdminApproval.isHidden = false
            likeBtn.isHidden = true
        }else{
            businessView.isHidden = true
            lblAdminApproval.isHidden = true
            lbladminapprovalHieght.constant = 0
            likeBtn.isHidden = false
        }
        optionView.isHidden = true
        businessColl.register(UINib(nibName: "BannerCollectionCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionCell")
    }

    @IBAction func editTap(_ sender: Any) {
        callBack?("edit")
    }
    @IBAction func deleteTap(_ sender: Any) {
        callBack?("delete")
    }
    
    @IBAction func likeTapped(_ sender: Any) {
        if likeBtn.currentImage == UIImage(named: "optionIC")

        {
            callBack?("option")
            likeBtn.backgroundColor = .lightGray
        }else{
            callBack?("like")
            likeBtn.backgroundColor = .white
        }
    }
    @IBAction func viewBusinessTap(_ sender: UIButton) {
        let vc = Storyboards.Other.instantiateViewController(withIdentifier: "BussinessDetailVC") as! BussinessDetailVC
//        vc.businessid = datagetApi[sender.tag].id ?? 0
        vc.businessid  = datagetBusinessHome[sender.tag].id ?? 0

        parentView?.navigationController?.pushViewController(vc, animated: true)
    }
   
    @IBAction func btnActive(_ sender: Any) {
        self.callbackActive?()
    }
    
}

extension BusinessDetailCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell( withReuseIdentifier: "BannerCollectionCell",for: indexPath) as! BannerCollectionCell
        cell.image.image = UIImage(named: "2")
        cell.image.layer.cornerRadius = 25
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
  
}
