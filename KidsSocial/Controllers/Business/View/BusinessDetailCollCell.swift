//
//  BusinessDetailCollCell.swift
//  KidsSocial
//
//  Created by AjayDhiman on 15/05/23.
//


import UIKit

class BusinessDetailCollCell: UICollectionViewCell {
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var businessDesc: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    
    @IBOutlet weak var businessImage: UIImageView!
    @IBOutlet weak var bussnessBtnHgt: NSLayoutConstraint!
    @IBOutlet weak var viewBusinessBtn: ThemeButton!
    @IBOutlet weak var optionView: UIView!
    @IBOutlet weak var activeButton: UIButton!
    //
    var role = "User"
    var callBack: ((String)->())?
    //
    override func awakeFromNib() {
        super.awakeFromNib()
        loadUI()
        // Initialization code
        
    }
    func loadUI(){
        if role == "User"{
            optionView.isHidden = true
            activeButton.isHidden = true
        }else{
            optionView.isHidden = false
            activeButton.isHidden = false
        }
    }
    
    @IBAction func editTap(_ sender: Any) {
        callBack?("edit")
    }
    
    @IBAction func deleteTap(_ sender: Any) {
        callBack?("edit")
    }
    
    @IBAction func likeTapped(_ sender: Any) {
        callBack?("like")
    }
    @IBAction func viewBusinessTap(_ sender: Any) {
        
    }
    
}
