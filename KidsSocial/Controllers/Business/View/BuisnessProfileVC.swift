//
//  BuisnessProfileVC.swift
//  KidsSocial
//
//  Created by cql131 on 15/06/23.
//

import UIKit
import SDWebImage

class BuisnessProfileVC: UIViewController {
    
    //MARK: - OUTLETS
    @IBOutlet weak var profileimage: UIImageView!
    @IBOutlet weak var namelblheader: UILabel!
   @IBOutlet weak var profileView: ShadowView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblMobile: UILabel!
    
    //MARK: - VARIABELS
    var viewmodal = AuthviewModel()
    var modal : getProfileModalBody?
    
    //MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        setup()
    }
    
    //MARK: - FUNCTIONS
    private func setup(){
        viewmodal.getprofile { data  in
            self.modal = data
            self.lblEmail.text = self.modal?.email ?? ""
            self.lblName.text = self.modal?.name ?? ""
            self.namelblheader.text = self.modal?.name ?? ""
            let imageIndex = (imageURL) + (self.modal?.image ?? "")
            self.profileimage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            self.profileimage.sd_setImage(with: URL(string: imageIndex), placeholderImage: UIImage(named: "pl"))
            self.lblMobile.text = self.modal?.phone ?? ""
            
        }
    }
    
    //MARK: - ACTIONSr
    @IBAction func editTapped(_ sender: UIButton){
        let vc = Storyboards.profile.instantiateViewController(withIdentifier: "EditProfile") as! EditProfile
        self.pushToVC(vc: vc)
    }

}
