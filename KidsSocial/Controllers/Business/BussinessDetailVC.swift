//
//  BussinessDetailVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 11/05/23.
//

import UIKit
import SDWebImage
import CoreAudio
import AVKit

class BussinessDetailVC: UIViewController {
    
    //MARK: - outlet
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var userMobile: UILabel!
    @IBOutlet weak var businessDetailVw: UIView!
    @IBOutlet weak var bannerColletion: UICollectionView!
    @IBOutlet weak var collhgt: NSLayoutConstraint!
    @IBOutlet weak var particularText: UILabel!
    @IBOutlet weak var transparentVW: UIView!
    @IBOutlet weak var particularColl: UICollectionView!
    @IBOutlet weak var businessDescription: UILabel!
    @IBOutlet weak var businessAddress: UILabel!
    @IBOutlet weak var userMail: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var likeBtn: UIButton!
    @IBOutlet weak var bussinessImg: UIImageView!
    
    //MARK: - variables
    var viewmodel = BuisnessviewModel()
    var datagetApi : BusinessDetailsModelBody?
    var businessid = Int()
    var datagetOriginal : Original?
    var datasimilar = [Original]()
    var datagetBannerHome = [BannerUser]()
    var arrdataLike = FavoriteModel()
    var dataBusiness = [BusinessUser]()
    //MARK: - viewdidload
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupApi()
        businessDetailVw.backgroundColor = .white
        businessDetailVw.layer.cornerRadius = 25
        likeBtn.setImage(UIImage(named: "dislike"), for: .normal)
        if isBusiness{
            businessDetailVw.isHidden = false
            likeBtn.isHidden = true
            particularText.isHidden = true
            collhgt.constant = 0
        }else{
            businessDetailVw.isHidden = true
            likeBtn.isHidden = false
            particularText.isHidden = true
        }
    }
    //MARK: - loadview
    override func loadView() {
        super.loadView()
        particularColl.register(UINib(nibName: "BusinessDetailCollCell", bundle: nil), forCellWithReuseIdentifier: "BusinessDetailCollCell")
        bannerColletion.register(UINib(nibName: "BannerCollectionCell", bundle: nil), forCellWithReuseIdentifier: "BannerCollectionCell")
        
        transparentVW.layer.cornerRadius = 25
        transparentVW.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
    }
    
    //MARK: - FUNCTIONS
    func setupApi(){
        viewmodel.buisnessDetailsapicall(business_id: self.businessid ) {  data in
//            self.businessDetailsModel = data
            self.datasimilar = data?.similar ?? []
            self.datagetApi = data
            
            if self.datagetApi?.original.isFavourite == 1{
                self.likeBtn.setImage(UIImage.init(named: "like"), for: .normal)
                
            }else{
                self.likeBtn.setImage(UIImage.init(named: "dislike"), for: .normal)
            }
         //   self.likeBtn.isSelected = self.datagetApi?.original.isFavourite == 0 ? false : true
            self.businessName.text = data?.original.name
            self.businessDescription.text = data?.original.shortDescription ?? ""
            self.userMobile.text = "\(data?.original.phone ?? 0)"
            self.businessAddress.text = data?.original.location ?? ""
            self.userName.text = data?.original.username
            self.userMail.text = data?.original.email
            self.userImg.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            self.userImg.setUserImage(URL(string: imageURL + (data?.original.userImage ?? "pl")))
            self.bussinessImg.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
            self.bussinessImg.setUserImage(URL(string: thambnailUrl + (data?.original.thumbnail ?? "")))
            self.particularColl.reloadData()
    
        }
    }
  
    //MARK: - action
    @IBAction func likeTapped(_ sender: UIButton) {
        if UserDefaults.standard.bool(forKey: "ok") == false{
            let alert = UIAlertController(title: "Login", message: "Please login first", preferredStyle: .alert)
                        let ok = UIAlertAction(title: "Login", style: .default, handler: { action in
                                let vc = Storyboards.main.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                self.pushToVC(vc: vc)
                            
                            
                             })
                             alert.addAction(ok)
                        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in self.dismiss(animated: true, completion: nil)
                             })
                             alert.addAction(cancel)
                             DispatchQueue.main.async(execute: {
                                self.present(alert, animated: true)
                        })
        
        }else{
            sender.isSelected  = !sender.isSelected
            let likeType = datagetApi?.original.isFavourite ?? 0 == 1 ? 2 : 1
            self.viewmodel.favUnfav(status: likeType, business_id: self.businessid) {
                
                    self.datagetApi?.original.isFavourite = likeType == 1 ? 1 : 2

               }
        }
   
//        sender.isSelected = !sender.isSelected
//        if self.datagetApi?.original.isFavourite == 0 ? false : true {
//            self.viewmodel.favUnfav(status: 2, business_id: businessid) {
//                self.likeBtn.setImage(UIImage(named: "dislike"), for: .normal)
//                self.setupApi()
//            }
//        } else{
//            self.viewmodel.favUnfav(status: 1, business_id: businessid) {
//                self.likeBtn.setImage(UIImage(named: "like"), for: .normal)
//                self.setupApi()
//            }
//        }
    }
    @IBAction func backTapped(_ sender: Any) {
        pop()
        }
    @IBAction func btnVidoTap(_ sender: Any) {
        let urlis = videoUrl + (datagetApi?.original.video ?? "")
                    print(urlis)
                    let url = URL(string: urlis)
                    let player = AVPlayer(url:url! )
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
    }
    }
}
//MARK: - uicollection delegate datasource
extension BussinessDetailVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == bannerColletion{
            return 2
        }else{
            if isBusiness{
                return 0
            }else{
                return datasimilar.count
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == bannerColletion{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BannerCollectionCell", for: indexPath) as! BannerCollectionCell
            cell.image.layer.cornerRadius = 25
            return cell
        }else{
            
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BusinessDetailCollCell", for: indexPath) as! BusinessDetailCollCell
        let data = datasimilar[indexPath.row]
            cell.businessName.text = data.name
            cell.businessDesc.text = data.location
           if indexPath.row == 0{
                cell.businessImage.setUserImage(URL(string: imageURL + (data.image ?? "")))

//
                   if self.datasimilar[indexPath.row].isFavourite == 1{
                       cell.likeBtn.setImage(UIImage.init(named: "like"), for: .normal)
                       
                   }else{
                       //               self.isLike = 2
                       cell.likeBtn.setImage(UIImage.init(named: "dislike"), for: .normal)
//                   }
               }
           }else{
                cell.businessImage.setUserImage(URL(string: videoUrl + (data.thumbnail ?? "")))
            }
            
        cell.viewBusinessBtn.addTarget(self, action: #selector(viewDetails(_:)), for: .touchUpInside)
        cell.role = "User"
       
            cell.callBack = { Void in
                
                
                if UserDefaults.standard.bool(forKey: "ok") == false{
                    let alert = UIAlertController(title: "Login", message: "Please login first", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "Login", style: .default, handler: { action in
                        let vc = Storyboards.main.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                        self.pushToVC(vc: vc)
                        
                        
                    })
                    alert.addAction(ok)
                    let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in self.dismiss(animated: true, completion: nil)
                    })
                    alert.addAction(cancel)
                    DispatchQueue.main.async(execute: {
                        self.present(alert, animated: true)
                    })
                    
                }else{
                    if self.datasimilar[indexPath.row].isFavourite ?? 0 == 1 {
                        self.viewmodel.favUnfav(status: 2, business_id: self.datasimilar[indexPath.row].id ?? 0) {
                            self.setupApi()
                        }
                    }else{
                        self.viewmodel.favUnfav(status: 1, business_id: self.datasimilar[indexPath.row].id ?? 0) {
                            self.setupApi()
                        }
                    }
                }
            }
        

        return cell
        }
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == bannerColletion{
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        }else{
        return CGSize(width: collectionView.frame.width/1.15, height: collectionView.frame.height)
        }
    }
    
    @objc func viewDetails(_ sender: UIButton){
        let vc = Storyboards.Other.instantiateViewController(withIdentifier: "BussinessDetailVC") as! BussinessDetailVC
        vc.businessid = self.datasimilar[sender.tag].id ?? 0
        pushToVC(vc: vc)
    }
}
