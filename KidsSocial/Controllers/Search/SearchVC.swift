//
//  SearchVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 11/05/23.
//

import UIKit
import SDWebImage

class SearchVC: UIViewController  {
    
    //    MARK: - OUTLETS
    @IBOutlet weak var searchTVC: UITableView!
    @IBOutlet weak var searchTxt: UITextField!
    
    //    MARK: - VARIABLE
    var viewmodel = UserViewModel()
    var datagetApi : [BuisnessGetModelBody]?
    var filterData : [BuisnessGetModelBody]?
    var dataGetApi = [BuisnessGetModelBody?]()
    var imageView: UIImageView?
    
   
    //    MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
      SetupApi()
        self.searchTVC.reloadData()
        searchTxt.delegate = self
//        searchTxt.becomeFirstResponder()
        searchTVC.register(UINib(nibName: "BusinessDetailCell", bundle: nil), forCellReuseIdentifier: "BusinessDetailCell")
    }
//MARK:- FUNCTION API
    func SetupApi(){
        viewmodel.buisnessGetApicall { data in
            self.dataGetApi = data ?? []
            self.datagetApi = data ?? []
            self.filterData = data ?? []
            self.searchTVC.reloadData()
        }
    }

}
extension SearchVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = filterData?.count ?? 0
                return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = searchTVC.dequeueReusableCell(withIdentifier: "BusinessDetailCell") as! BusinessDetailCell
        
        let item = filterData?[indexPath.row]
        cell.viewBusinessBtn.isHidden = true
        cell.bussnessBtnHgt.constant = 0
        cell.businessName.text = item?.name
        cell.locationlbl.text = item?.location
        cell.Businessimage.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        cell.Businessimage.setUserImage(URL(string: imageURL + (item?.image ?? "")))
        cell.selectionStyle = .none
        cell.likeBtn.isHidden = true
        cell.businessView.isHidden = true
        cell.activeButton.isHidden = true
        cell.optionView.isHidden = true
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Storyboards.Other.instantiateViewController(withIdentifier: "BussinessDetailVC") as! BussinessDetailVC
//        vc.businessid = datagetApi[sender.tag].id ?? 0
        vc.businessid  = filterData?[indexPath.row].id ?? 0

        self.navigationController?.pushViewController(vc, animated: true)
    }
}


//MARK: - EXTENSIONS
extension SearchVC: UITextFieldDelegate {
    //MARK:- UITextField Delegate Methods
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var search = textField.text ?? ""
        if string.isEmpty {
            search = String(search.dropLast())
        } else {
            search = textField.text!+string
        }
        print(search)
        if search == "" {
            self.filterData = self.datagetApi
//            self.filterData?.removeAll()
        } else {
            
            self.filterData = datagetApi?.filter{($0.name ?? "").lowercased()
                .contains(search.lowercased())}
          
        }
        
        self.searchTVC.reloadData()
        return true
    }
}
