//
//  FavouriteVC.swift
//  KidsSocial
//
//  Created by AjayDhiman on 11/05/23.
//

import UIKit
import SDWebImage
import SwiftGifOrigin

class FavouriteVC: UIViewController {
//MARK: - OUTLETS
    @IBOutlet weak var imgGif: UIImageView!
    @IBOutlet weak var favouriteTVC: UITableView!
    
//    MARK: - VARIABLE
    var viewmodel = UserViewModel()
    var datagetApi = [FavListingtModelBody]()
    var dataBusiness = [BusinessUser]()
    var viewmodelsecond = BuisnessviewModel()
    var imageView: UIImageView?
    var srr = [1,1,1]
    
//    MARK: - LIFE CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        if Store.userDetails == nil {
            if UserDefaults.standard.bool(forKey: "ok") == false{
                let alert = UIAlertController(title: "Login", message: "Please login first", preferredStyle: .alert)
                let ok = UIAlertAction(title: "Login", style: .default, handler: { action in
                    let vc = Storyboards.main.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    self.pushToVC(vc: vc)
                    
                    
                })
                alert.addAction(ok)
                let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
                    //                            self.navigationController?.popViewController(animated: false)
                    let vc = Storyboards.tab.instantiateViewController(withIdentifier: "TabBarVC") as! TabBarVC
                    self.navigationController?.pushViewController(vc, animated: false)
                    
                })
                alert.addAction(cancel)
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true)
                })
                
            }
        }else{
            setupAPI()
        }
        favouriteTVC.delegate = self
        favouriteTVC.dataSource = self
        
        
    }
    override func loadView(){
        super.loadView()
        favouriteTVC.register(UINib(nibName: "BusinessDetailCell", bundle: nil), forCellReuseIdentifier: "BusinessDetailCell")
    }
//    MARK: - FUNCTIONS
   func  setupAPI(){
       viewmodel.favouriteListing { data in
           self.datagetApi = data ?? []
           self.favouriteTVC.reloadData()

           if self.datagetApi.count == 0 {
                   self.showBackgroundGIF()
               
           } else {
                   self.removeBackgroundGIF()
               
           }
       }
    }
    func showBackgroundGIF() {
               let gifURL = Bundle.main.url(forResource: "Nodatafound", withExtension: "gif") // Replace "noDataGif" with the name of your GIF file

               if let gifURL = gifURL, let gifData = try? Data(contentsOf: gifURL), let source = CGImageSourceCreateWithData(gifData as CFData, nil) {
                   let frameCount = CGImageSourceGetCount(source)
                   var images: [UIImage] = []

                   for i in 0..<frameCount {
                       if let cgImage = CGImageSourceCreateImageAtIndex(source, i, nil) {
                           let uiImage = UIImage(cgImage: cgImage)
                           images.append(uiImage)
                       }
                   }

                   imageView = UIImageView(frame: favouriteTVC.bounds)
                   imageView?.animationImages = images
                   imageView?.animationDuration = TimeInterval(frameCount) * 0.1 // Adjust the animation speed if needed
                   imageView?.animationRepeatCount = 0 // Repeat indefinitely
                   imageView?.contentMode = .scaleAspectFit
                   imageView?.startAnimating()

                   favouriteTVC.backgroundView = imageView
                   favouriteTVC.separatorStyle = .none
               }
           }

           func removeBackgroundGIF() {
               imageView?.stopAnimating()
               //imageView? = nil
               favouriteTVC.backgroundView = nil
               favouriteTVC.separatorStyle = .singleLine
           }
}
//MARK:-  TABLE VIEW
extension FavouriteVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datagetApi.count ?? 0
    }

   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessDetailCell", for: indexPath) as! BusinessDetailCell
        let data = datagetApi[indexPath.row]

        cell.locationlbl.text = data.location
        cell.viewBusinessBtn.isHidden = true
        cell.bussnessBtnHgt.constant = 0
        cell.businessName.text = data.name
        cell.Businessimage.setUserImage(URL(string: imageURL + (data.image ?? "")))
        cell.selectionStyle = .none
        cell.likeBtn.setImage(UIImage(named: "like"), for: .normal)
        cell.callBack = { data in
                       if data == "like"{
                           self.viewmodelsecond.favUnfav(status: 2, business_id: self.datagetApi[indexPath.row].id ?? 0) {
                               self.datagetApi.remove(at: indexPath.row)
                               self.favouriteTVC.reloadData()
                               
                               if self.datagetApi.count == 0 {
                                   self.showBackgroundGIF()
                                   
                               } else {
                                   self.removeBackgroundGIF()
                                   
                               }
                           }

                        }
                
            }
        cell.activeButton.isHidden = true
        cell.optionView.isHidden = true
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Storyboards.Other.instantiateViewController(withIdentifier: "BussinessDetailVC") as! BussinessDetailVC
//        vc.businessid = datagetApi[sender.tag].id ?? 0
        vc.businessid  = datagetApi[indexPath.row].id ?? 0

        self.navigationController?.pushViewController(vc, animated: true)
    }
}

