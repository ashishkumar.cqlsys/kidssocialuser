//
//  SubBusinessVC.swift
//  KidsSocial
//
//  Created by cqlpc on 07/07/23.
//

import UIKit
import SDWebImage
import SwiftGifOrigin
class SubBusinessVC: UIViewController {

    @IBOutlet weak var imgGif: UIImageView!
    @IBOutlet weak var tblVW: UITableView!
    var arrcategoryId = Int()
    var arrsubcategory = Int()
    var viewmodel = UserViewModel()
    var datagetmodel : [SubBusinessModelBody]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
setupAPi()
      
    }
//FUNCTION API CALL
    func setupAPi() {
        viewmodel.subcategoryListingapicall(category_id: self.arrcategoryId, sub_category_id: self.arrsubcategory) { data in
            self.datagetmodel = data
            self.tblVW.reloadData()
        }
    }
    @IBAction func btnback(_ sender: Any) {
        self.pop()
    }
}
//MARK- : EXTENSION  TABLE VIEW
extension SubBusinessVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if datagetmodel?.count == 0{
            imgGif.image = UIImage.gif(name: "Nodatafound")
            imgGif.isHidden = false
                }else{
                    imgGif.isHidden = true
                    tblVW.backgroundView = nil
                   
                }
        return datagetmodel?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubBusinessTVC", for: indexPath) as! SubBusinessTVC
        let data = datagetmodel?[indexPath.row]
        cell.lbllocation.text =  data?.location
        cell.lblname.text = data?.name
        cell.imgbusiness.sd_imageIndicator = SDWebImageActivityIndicator.grayLarge
        cell.imgbusiness.setUserImage(URL(string: imageURL + (data?.image ?? "")))
        return cell
     }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = Storyboards.Other.instantiateViewController(withIdentifier: "BussinessDetailVC") as! BussinessDetailVC
        vc.businessid  = datagetmodel?.first?.id ?? 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
