//
//  Storyboard.swift
//  KidsSocial
//
//  Created by AjayDhiman on 10/05/23.
//

import Foundation
import UIKit

struct Storyboards{
    public static var main = UIStoryboard(name: "Main", bundle: nil)
    public static var profile = UIStoryboard(name: "Profile", bundle: nil)
    public static var tab = UIStoryboard(name: "TabBar", bundle: nil)
    public static var Other = UIStoryboard(name: "Other", bundle: nil)
    public static var business = UIStoryboard(name: "Business", bundle: nil)
}
