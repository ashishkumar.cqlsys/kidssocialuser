//
//  AppDelegate.swift
//  KidsSocial
//
//  Created by AjayDhiman on 09/05/23.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import GoogleSignIn
import GooglePlaces

@main
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate {
    private let locationManager = CLLocationManager()
    var locationUpdated = Bool()
 
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        sleep(2)
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationUpdated = true
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }else{
            locationUpdated = false
        }
//        GIDSignIn.sharedInstance.restorePreviousSignIn { user, error in
//            if error != nil || user == nil {
//                GIDSignIn.sharedInstance.signOut()
//                
//            } else {
//                GIDSignIn.sharedInstance.hasPreviousSignIn()
//            }
//        }

        GMSPlacesClient.provideAPIKey("AIzaSyDQWqIXO-sNuMWupJ7cNNItMhR4WOkzXDE")
        IQKeyboardManager.shared.enable = true
        UIApplication.shared.applicationIconBadgeNumber = 0
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        GIDSignIn.sharedInstance().clientID = googleSignInClientKey
        application.applicationIconBadgeNumber = 0
        
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        registerForPushNotifications()
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "KidsSocial")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
//    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//
//        if let dict = response.notification.request.content.userInfo as? [String:Any]{
//            print(dict)
//            completionHandler()
//        }
//    }
}
    extension AppDelegate: UNUserNotificationCenterDelegate{
        
        func registerForPushNotifications() {
            
            UNUserNotificationCenter.current().delegate = self
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                (granted, error) in
                print("Permission granted: \(granted)")
                // 1. Check if permission granted
                guard granted else { return }
                // 2. Attempt registration for remote notifications on the main thread
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
        
        func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
            // 1. Convert device token to string
            let tokenParts = deviceToken.map { data -> String in
                return String(format: "%02.2hhx", data)
            }
            let token = tokenParts.joined()
            // 2. Print device token to use for PNs payloads
            print("Device Token: \(token)")
            // Store.deviceToken = token
            DEVICE_TOKEN = token
        }
        
        func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
            // 1. Print out error if PNs registration not successful
            print("Failed to register for remote notifications with error: \(error)")
            //Store.deviceToken = "simulator/error"
            DEVICE_TOKEN = "simulator/error"
        }
        
        func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
        {
           // if UIApplication.shared.applicationState == .background || UIApplication.shared.applicationState == .inactive || UIApplication.shared.applicationState == .active  {
                completionHandler([.badge,.banner,.sound])
                
                
           // }
        }
         func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
             
            if let userInfo = response.notification.request.content.userInfo as? [String:Any] {
                print("**********************")
                print(userInfo)
                
                let apnsData = userInfo["body"] as? [String:Any]
                let id = apnsData?["id"] as? String?
                let notificationType = apnsData?["code"] as? Int
                print("dataaa is",apnsData)
                print("typee is-----",notificationType)
                
                if notificationType == 4 {
                    let storyboard1 = UIStoryboard.init(name: "TabBar", bundle: Bundle.main) as? UIStoryboard
                    let stry = UIStoryboard.init(name: "Profile", bundle: Bundle.main) as? UIStoryboard
                    let Home = storyboard1?.instantiateViewController(withIdentifier: "BusinessTabBarVC") as! BusinessTabBarVC
                    let notification = stry?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
                    let navigationController = UINavigationController(rootViewController: notification)
                    navigationController.navigationBar.isHidden = true
                    navigationController.viewControllers = [Home,notification]
                    UIApplication.shared.windows.first?.rootViewController = navigationController
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                }else if notificationType == 5{
                    let storyboard1 = UIStoryboard.init(name: "TabBar", bundle: Bundle.main) as? UIStoryboard
                    let stry = UIStoryboard.init(name: "Profile", bundle: Bundle.main) as? UIStoryboard
                    let Home = storyboard1?.instantiateViewController(withIdentifier: "BusinessTabBarVC") as! BusinessTabBarVC
                    let notification = stry?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
                    let navigationController = UINavigationController(rootViewController: notification)
                    navigationController.navigationBar.isHidden = true
                    navigationController.viewControllers = [Home,notification]
                    UIApplication.shared.windows.first?.rootViewController = navigationController
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                }
                else if notificationType == 7{
                    let storyboard1 = UIStoryboard.init(name: "TabBar", bundle: Bundle.main) as? UIStoryboard
                    let stry = UIStoryboard.init(name: "Profile", bundle: Bundle.main) as? UIStoryboard
                    let Home = storyboard1?.instantiateViewController(withIdentifier: "BusinessTabBarVC") as! BusinessTabBarVC
                    let notification = stry?.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
                    let navigationController = UINavigationController(rootViewController: notification)
                    navigationController.navigationBar.isHidden = true
                    navigationController.viewControllers = [Home,notification]
                    UIApplication.shared.windows.first?.rootViewController = navigationController
                    UIApplication.shared.windows.first?.makeKeyAndVisible()
                }
            }
        }
    }




