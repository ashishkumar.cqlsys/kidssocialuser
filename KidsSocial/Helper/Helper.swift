//
//  Helper.swift
//  KidsSocial
//
//  Created by AjayDhiman on 09/05/23.
//

import Foundation
import UIKit
import AVFoundation

//
class ThemeButton: UIButton{
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUP()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUP()
    }
    
    func setUP(){
        self.layer.cornerRadius = 25
        self.layer.borderColor = UIColor(named: "themeColor")?.cgColor
        self.layer.borderWidth = 1
        self.setTitleColor(.white, for: .normal)
        self.backgroundColor = UIColor(named: "themeColor")
        guard let font = UIFont(name: "Helvetica", size: 16)?.fontDescriptor.withSymbolicTraits(.traitBold) else{return}
        self.titleLabel?.font = UIFont(descriptor: font, size: 16)


    }
}
//
class PinkBorderBtn: UIButton{
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUP()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUP()
    }
    
    func setUP(){
        self.layer.cornerRadius = 25
        self.layer.borderColor = UIColor(named: "themeColor")?.cgColor
        self.layer.borderWidth = 1
        self.setTitleColor(UIColor(named: "themeColor"), for: .normal)
        self.backgroundColor = .white
        guard let font = UIFont(name: "Helvetica", size: 16)?.fontDescriptor.withSymbolicTraits(.traitBold) else{return}
        self.titleLabel?.font = UIFont(descriptor: font, size: 16)

    }
}
//
class BorderBtn: UIButton{
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUP()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUP()
    }
    
    func setUP(){
        self.layer.cornerRadius = 25
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 1
        self.setTitleColor(.black, for: .normal)
        self.backgroundColor = .white
        guard let font = UIFont(name: "Helvetica", size: 16)?.fontDescriptor.withSymbolicTraits(.traitBold) else{return}
        self.titleLabel?.font = UIFont(descriptor: font, size: 16)

    }
}
//MARK: - uiview
class TextfeildView: UIView{
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUP()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUP()
    }
    
    func setUP(){
        self.layer.cornerRadius = 25
        self.backgroundColor = UIColor(named: "textFieldBG")
    }
}
//MARK: - shadowView
class ShadowView: UIView{
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUP()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUP()
    }

    func setUP(){
        self.layer.cornerRadius = 25
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowRadius = 2
        self.layer.shadowOffset = CGSize(width: 0.2, height: 0.3)
        self.layer.shadowOpacity = 0.5
    }
}
//MARK: - headerLabel
class HeaderLabel: UILabel{
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUP()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUP()
    }
    
    func setUP(){
        guard let font = UIFont(name: "Helvetica", size: 16)?.fontDescriptor.withSymbolicTraits(.traitBold) else
        {
            self.font = UIFont(name: "Helvetica", size: 20)
            return
        }
        self.font = UIFont(descriptor: font, size: 16)
    }
}
//MARK: - tabbarTitlecolor
func selectedTitleColor() {
    if let themeColor = UIColor(named: "themeColor") {
        let selectedTitleAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: themeColor]
        UITabBarItem.appearance().setTitleTextAttributes(selectedTitleAttributes, for: .selected)
    }
}

